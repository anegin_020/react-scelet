import { createStore, applyMiddleware, compose } from 'redux';
import { fromJS } from 'immutable';
import { createLogger } from 'redux-logger';
import createSagaMiddleware, { END } from 'redux-saga';
import { routerMiddleware } from 'react-router-redux';
import DevTools from '../containers/DevTools';
import createReducer from '../reducers';

export default function configureStore(initialState, history) {
    const sagaMiddleware = createSagaMiddleware();

    const middlewares = [
        sagaMiddleware,
        routerMiddleware(history),
        //createLogger()
    ];

    const enhancers = [
        applyMiddleware(...middlewares),
        DevTools.instrument()
    ];

    const composeEnhancers =
        process.env.NODE_ENV !== 'production' &&
        typeof window === 'object' &&
        window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ?
            window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ : compose;

    const store = createStore(
        createReducer(),
          fromJS(initialState),
        composeEnhancers(...enhancers)
    );

    if (module.hot) {
        module.hot.accept('../reducers', () => {
            import('../reducers').then((reducerModule) => {
                const createReducers = reducerModule.default;
                const nextReducers = createReducers(store.asyncReducers);

                store.replaceReducer(nextReducers);
            });
        })
    }

    store.runSaga = sagaMiddleware.run;
    store.asyncReducers = {};

    return store
}
