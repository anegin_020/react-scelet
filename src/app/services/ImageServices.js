import {api} from '../utils/ApiUtil';

export function loadRandomImageService(apiKey){
    return api(`https://api.desktoppr.co/1/users/keithpitt/wallpapers/random`);
}