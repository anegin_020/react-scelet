import React from 'react';
import ReactDOM from 'react-dom';
import {syncHistoryWithStore} from 'react-router-redux';
import {createBrowserHistory} from 'history';
import FontFaceObserver from 'fontfaceobserver';
import { translationMessages } from './i18n';
import createRoutes from './router';
import Root from './containers/Root';
import configureStore from './store';
import RootSaga from './sagas/RootSagas';
import { makeLocationState } from './containers/App/selectors';
import "../assets/styles/index.css";

const openSansObserver = new FontFaceObserver('Open Sans', {});
openSansObserver.load().then(() => {
  document.body.classList.add('fontLoaded');
}, () => {
  document.body.classList.remove('fontLoaded');
});

const initialState = {};
const store = configureStore(initialState, createBrowserHistory());
store.runSaga(RootSaga);

const history = syncHistoryWithStore(createBrowserHistory(), store, {
  selectLocationState: makeLocationState(),
});

const render = (messages) => {
  ReactDOM.render(
    <Root
      store={store}
      history={history}
      createRoutes={createRoutes}
      messages={messages}/>,
    document.getElementById('app')
  )
};

if (module.hot) {
  module.hot.accept('./i18n', () => {
    render(translationMessages);
  });
}

if (!window.Intl) {
  (new Promise((resolve) => {
    resolve(import('react-intl'));
  }))
    .then(() => Promise.all([
      import('react-intl/locale-data/en.js'),
      import('react-intl/locale-data/ru.js'),
    ]))
    .then(() => render(translationMessages))
    .catch((err) => {
      throw err;
    });
} else {
  render(translationMessages);
}
