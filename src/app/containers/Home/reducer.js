import {fromJS} from 'immutable';
import {
  CHANGE_USERNAME,
}  from './constants';

const initState = fromJS({
  homeInit: false,
});

export default function(state = initState, action) {
  switch(action.type){
    case CHANGE_USERNAME:
      return state.set('homeInit', true);
    default:
      return state;
  }
}
