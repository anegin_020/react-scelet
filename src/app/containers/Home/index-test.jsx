import React, {Component, PureComponent} from 'react'
import PropTypes from 'prop-types'
import {fromJS} from 'immutable'
import {connect} from 'react-redux'
//import {Link} from 'react-router-dom'
import forEach from 'lodash/forEach'
import ScrollToTopOnMount from '../../components/utils/ScrollToTopOnMount'
import {loadImagesAction} from '../../actions/ImageActions.js'
import {convertToHTML} from 'draft-convert'
import renderHTML from 'react-render-html'
import {stateToHTML} from 'draft-js-export-html'
import HashtagPlugin from '../../components/RichEditor/plugins/Hashtag'
//import Editor from 'draft-js-plugins-editor'
import {
  Editor,
  EditorState,
  Modifier,
  RichUtils,
  CompositeDecorator,
  convertToRaw,
  convertFromHTML,
  convertFromRaw,
  ContentState,
  getDefaultKeyBinding,
  KeyBindingUtil
} from 'draft-js'
import EditorToolBar from '../../components/RichEditor/utils/ToolBar'
import LinkDecorator from '../../components/RichEditor/decorators/LinkDecorator'

const styles = {
  hashtag: {
    color: 'rgb(0, 129, 255)',
    direction: 'ltr',
    unicodeBidi: 'bidi-override',
    fontSize: '0.9em',
  },
  handle: {
    color: 'rgb(0, 86, 78)',
    direction: 'ltr',
    unicodeBidi: 'bidi-override',
    fontSize: '0.9em',
  },
  body: {
    background: 'transparent',
    fontFamily: '"Lato", serif',
    fontSize: '1em',
    padding: '0 20px',
  },
  editor: {
    background: '#fff',
    border: '1px solid #ccc',
    padding: '20px 10px',
    cursor: 'text',
    fontSize: '1em',
    margin: '10px 0',
    fontFamily: "Lato",
    transition: 'all 0.2s ease-in',
  },
}

const HANDLE_REGEX = /\@[\w]+/g
const STAR_REGEX = /\#[\w\u0590-\u05ff]+/g

const kindOfMarkdownPlugin = HashtagPlugin([{
  strategy: (contentBlock, callback) => findWithRegex(STAR_REGEX, contentBlock, callback),
  styles: ['BOLD']
},
]);

const HandleSpan = (props) => {
  return <span style={styles.handle}>{props.children}</span>;
};

const findWithRegex = (regex, contentBlock, callback) => {
  const text = contentBlock.getText();
  let matchArr, start;
  while ((matchArr = regex.exec(text)) !== null) {
    start = matchArr.index;
    callback(start, start + matchArr[0].length);
  }
}

const findLinkEntities = (contentBlock, callback, contentState) => {
  console.log(contentBlock, callback, contentState);
  contentBlock.findEntityRanges(
    (character) => {
      const entityKey = character.getEntity();
      return (
        entityKey !== null &&
        contentState.getEntity(entityKey).getType() === 'LINK'
      );
    },
    callback
  );
}

const handleLink = (props) => {
  const {url} = props.contentState.getEntity(props.entityKey).getData();
  return (
    <a href={url} style={styles.link}>
      {props.children}
    </a>
  );
};

const handleStrategy = (contentBlock, callback, contentState) => {
  findWithRegex(HANDLE_REGEX, contentBlock, callback)
}

const decorator = new CompositeDecorator([
  /*{
    strategy: handleStrategy,
    component: HandleSpan,
  },*/
  LinkDecorator,
]);

const styleMap = {
  CODE: {
    display: 'inline-block',
    backgroundColor: 'rgba(0, 0, 0, 0.05)',
    fontFamily: '"Inconsolata", "Menlo", "Consolas", monospace',
    fontSize: 16,
    padding: 2,
    marginTop: 3
  },
}

class RichEditor extends Component {

  static propTypes = {
    readOnly: PropTypes.bool,
  }

  static defaultProps = {
    readOnly: false,
  }

  constructor(props) {
    super(props)
    this.focus = () => this.refs.editor.focus()
  }

  static createWithRowContent(rowContent) {
    forEach(rowContent.entityMap, function(value, key) {
      value.data.mention = fromJS(value.data.mention)
    })
    const contentState = convertFromRaw(rowContent)
    return EditorState.createWithContent(contentState, decorator)
  }

  static createWithHTMLContent(htmlContent) {
    const blocksFromHTML = convertFromHTML(htmlContent);
    const state = ContentState.createFromBlockArray(
      blocksFromHTML.contentBlocks,
      blocksFromHTML.entityMap,
    );

    return EditorState.createWithContent(state, decorator)
  }

  static createEmptyContent() {
    return EditorState.createEmpty(decorator);
  }

  static renderContent(currentState) {
    const rowState = convertToRaw(currentState);
    console.log(rowState);
    console.log(JSON.stringify(rowState));
    return 'Nothing';
  }

  customKeyHandler = (event) => {
    const {hasCommandModifier} = KeyBindingUtil;
    /*if (event.keyCode === 83 /!* `S` key *!/ && hasCommandModifier(event)) {
      return 'myeditor-save';
    }*/
    return getDefaultKeyBinding(event);

  }

  focus = () => {
    this.refs.editor.focus();
  }

  handleKeyCommand = (command) => {
    const {editorState, onChange} = this.props;
    const newState = RichUtils.handleKeyCommand(editorState, command);
    if (newState) {
      onChange(newState);
      return true;
    }
    return false;
  }

  render() {
    const {editorState, onChange, readOnly} = this.props;
    let editorToolbar = null;
    if(readOnly === false){
      editorToolbar = <EditorToolBar
        editorState={editorState}
        onChange={onChange}
        focusEditor={this.focus}
      />
    }
    return <div style={styles.body}>
      {editorToolbar}
      <div style={styles.editor} onClick={this.focus}>
        <Editor
          editorState={editorState}
          keyBindingFn={this.customKeyHandler}
          handleKeyCommand={this.handleKeyCommand}
          customStyleMap={styleMap}
          onChange={onChange}
          placeholder="Be cool."
          ref="editor"
          spellCheck={true}
          /*plugins={ [kindOfMarkdownPlugin] }*/
          readOnly={readOnly}
        />
      </div>
    </div>
  }
}

export default class Home extends Component {

  constructor(props) {
    super(props);
    let ht = {"entityMap":{"0":{"type":"LINK","mutability":"MUTABLE","data":{"url":"/sections","reactive":true}},"1":{"type":"LINK","mutability":"MUTABLE","data":{"url":"http://onliner.by","reactive":false}}},"blocks":[{"key":"c4jl0","text":" createSelectorCreator(memoize, ...memoizeOptions)","type":"unstyled","depth":0,"inlineStyleRanges":[{"offset":1,"length":49,"style":"BOLD"}],"entityRanges":[],"data":{}},{"key":"39ele","text":"createSelectorCreator can be used to make a customized version of createSelector.","type":"unstyled","depth":0,"inlineStyleRanges":[{"offset":0,"length":21,"style":"CODE"},{"offset":66,"length":14,"style":"CODE"}],"entityRanges":[],"data":{}},{"key":"8edc6","text":"The memoize argument is a memoization function to replace defaultMemoize.","type":"unstyled","depth":0,"inlineStyleRanges":[{"offset":4,"length":7,"style":"CODE"},{"offset":58,"length":14,"style":"CODE"}],"entityRanges":[],"data":{}},{"key":"593gu","text":"It's new link","type":"unstyled","depth":0,"inlineStyleRanges":[],"entityRanges":[{"offset":0,"length":13,"key":0}],"data":{}},{"key":"aqskp","text":"It's no react link","type":"unstyled","depth":0,"inlineStyleRanges":[],"entityRanges":[{"offset":0,"length":18,"key":1}],"data":{}}]}

    this.state = {
      richTextState: RichEditor.createWithRowContent(ht),
      renderText: '',
    };
  }

  handleChangeRichEditor = (EditorState) => {
    this.setState({
      richTextState: EditorState,
    })
  };

  handleClick = () => {
    this.setState({renderText: RichEditor.renderContent(this.state.richTextState.getCurrentContent())})
  };

  render() {
    const {request} = this.props;
    const {richTextState, renderText} = this.state;
    return (
      <div>
        <ScrollToTopOnMount/>
        <h1>Home page</h1>
        <RichEditor onChange={this.handleChangeRichEditor} editorState={richTextState} readOnly={false}/>
        <br/>
        <div style={{background: 'green'}}>
          <RichEditor onChange={this.handleChangeRichEditor} editorState={richTextState} readOnly={true}/>
        </div>
        <br/>
        <h1 onClick={this.handleClick}>Click for render content</h1>
        <br/>
        {renderText}
      </div>
    );
  }
}
