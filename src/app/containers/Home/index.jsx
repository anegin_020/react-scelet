import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
//import {Link} from 'react-router-dom';
import _ from 'lodash'
import ScrollToTopOnMount from '../../components/utils/ScrollToTopOnMount';
import {loadImagesAction} from '../../actions/ImageActions.js';
import {convertToHTML} from 'draft-convert';
import renderHTML from 'react-render-html';
import {stateToHTML} from 'draft-js-export-html';
import _styles from './index.css';
import {Editor, EditorState, Modifier, RichUtils, CompositeDecorator, convertToRaw, convertFromRaw, Entity} from 'draft-js';
import AnswerCommentDecorator from '../../components/RichEditor/decorators/AnswerCommentDecorator'
import {ENTITY_TYPE} from '../../components/RichEditor/constants/index'

const TAG_REGEX = /\@[\w]+/g;
function tagHandle(contentBlock, callback) {
  findWithRegex(TAG_REGEX, contentBlock, callback);
}

function findWithRegex(regex, contentBlock, callback) {
  const text = contentBlock.getText();
  let matchArr, start;
  while ((matchArr = regex.exec(text)) !== null) {
    start = matchArr.index;
    callback(start, start + matchArr[0].length);
  }
}

const styles = {
  tag: {
    color: 'rgba(98, 177, 254, 1.0)',
    direction: 'ltr',
    unicodeBidi: 'bidi-override',
  }
}

const Tag = (props) => {
  return <span {...props} style={styles.tag}>{props.children}</span>;
}

const styleMap = {
  CODE: {
    display: 'inline-block',
    backgroundColor: 'rgba(0, 0, 0, 0.05)',
    fontFamily: '"Inconsolata", "Menlo", "Consolas", monospace',
    fontSize: 16,
    padding: 2,
    marginTop: 3
  },
  LINK: {
    color: '#76daff'
  }
};

class StyleButton extends React.Component {
  constructor() {
    super();
    this.onToggle = (e) => {
      e.preventDefault();
      this.props.onToggle(this.props.style);
    };
  }

  render() {
    let className = 'CodepenCommentEditor-styleButton';
    if (this.props.active) {
      className += ' CodepenCommentEditor-activeButton';
    }

    return (
      <span className={className} onMouseDown={this.onToggle}>
        {this.props.label}
      </span>
    );
  }
}

function getBlockStyle(block) {
  switch (block.getType()) {
    case 'blockquote': return 'CodepenCommentEditor-blockquote';
    default: return null;
  }
}

const BLOCK_TYPES = [
  {label: 'Quote', style: 'blockquote'},
  {label: 'Block Code', style: 'code-block'},
];

const BlockStyleControls = (props) => {
  const {editorState} = props;
  const selection = editorState.getSelection();
  const blockType = editorState
    .getCurrentContent()
    .getBlockForKey(selection.getStartKey())
    .getType();

  return (
    <span className="CodepenCommentEditor-controls">
      {BLOCK_TYPES.map((type) =>
        <StyleButton
          key={type.label}
          active={type.style === blockType}
          label={type.label}
          onToggle={props.onToggle}
          style={type.style}
        />
      )}
      <StyleButton
        label="decorator"
        onToggle={props.onDecorator}
        style={ENTITY_TYPE.ANSWER_COMMENT}
      />
    </span>
  );
};

var INLINE_STYLES = [
  {label: 'Bold', style: 'BOLD'},
  {label: 'Italic', style: 'ITALIC'},
  {label: 'Link', style: 'LINK'},
  {label: 'Inline Code', style: 'CODE'},
];

const InlineStyleControls = (props) => {
  var currentStyle = props.editorState.getCurrentInlineStyle();
  return (
    <span className="CodepenCommentEditor-controls">
      {INLINE_STYLES.map(type =>
        <StyleButton
          key={type.label}
          active={currentStyle.has(type.style)}
          label={type.label}
          onToggle={props.onToggle}
          style={type.style}
        />
      )}
    </span>
  );
};

class ColorfulEditorExample extends React.Component {
  constructor(props) {
    super(props);

    // Defining decorator
    const compositeDecorator = new CompositeDecorator([
      {
        strategy: tagHandle,
        component: Tag,
      },
      AnswerCommentDecorator({props: {editor: this}}),
    ]);
    const html = convertFromRaw({"entityMap":{"0":{"type":"LINK","mutability":"MUTABLE","data":{"url":"qwerty","react":false}}},"blocks":[{"key":"d9819","text":"abc","type":"unstyled","depth":0,"inlineStyleRanges":[],"entityRanges":[{"offset":0,"length":3,"key":0}],"data":{}}]})
    // Connect the decorator to the obj entity
    this.state = {editorState: EditorState.createWithContent(html,compositeDecorator)};
    this.state.editorState = EditorState.set(this.state.editorState, { decorator:compositeDecorator });
    this.focus = () => this.refs.editor.focus();
    this.onChange = (editorState) => {
      this.props.onRender(convertToRaw(editorState.getCurrentContent()))
      this.setState({editorState})
    };

    this.handleKeyCommand = (command) => this._handleKeyCommand(command);
    this.toggleBlockType = (type) => this._toggleBlockType(type);
    this.toggleInlineStyle = (style) => this._toggleInlineStyle(style);
  }

  _handleKeyCommand(command) {
    const {editorState} = this.state;
    const newState = RichUtils.handleKeyCommand(editorState, command);
    if (newState) {
      this.onChange(newState);
      return true;
    }
    return false;
  }

  _toggleBlockType(blockType) {
    this.onChange(
      RichUtils.toggleBlockType(
        this.state.editorState,
        blockType
      )
    );
  }

  _toggleInlineStyle(inlineStyle) {
    this.onChange(
      RichUtils.toggleInlineStyle(
        this.state.editorState,
        inlineStyle
      )
    );
  }

  onDecorator = () => {
    let {editorState} = this.state;
    let contentState = editorState.getCurrentContent();
    let selection = editorState.getSelection();
    let entityKey = Entity.create(ENTITY_TYPE.ANSWER_COMMENT, 'IMMUTABLE', {
      senderUrl: '#',
      senderThumb: 'http://i866.photobucket.com/albums/ab230/nahank/tv20in20%20round%2013/70x70.jpg',
      urlComment: '#',
      nickname: 'Вася Пупкин'
    });
    const updatedContent = Modifier.insertText(contentState, selection, ' ', null, entityKey);

    this.onChange(
      EditorState.push(editorState, updatedContent)
    );
    this.focus();
  }

  render() {
    const {editorState} = this.state;
    let className = 'CodepenCommentEditor-editor';
    var contentState = editorState.getCurrentContent();
    if (!contentState.hasText()) {
      if (contentState.getBlockMap().first().getType() !== 'unstyled') {
        className += ' CodepenCommentEditor-hidePlaceholder';
      }
    }

    var selectionState = editorState.getSelection();

    if(selectionState.getHasFocus()){
      className += ' CodepenCommentEditor-focus';
    }

    return (
      <div className="CodepenCommentEditor-root">
        <InlineStyleControls
          editorState={editorState}
          onToggle={this.toggleInlineStyle}
        />
        <BlockStyleControls
          editorState={editorState}
          onToggle={this.toggleBlockType}
          onDecorator={this.onDecorator}
        />
        <div className={className} onClick={this.focus}>
          <Editor
            blockStyleFn={getBlockStyle}
            customStyleMap={styleMap}
            editorState={editorState}
            handleKeyCommand={this.handleKeyCommand}
            onChange={this.onChange}
            placeholder="Be cool."
            ref="editor"
            spellCheck={true}
          />
        </div>
      </div>
    );
  }
}

export default class Home extends Component {

  constructor(props) {
    super(props);
    this.state = {
      editorRender: '',
    };
  }

  static containsSome(haystack, needles) {
    return haystack.length > _.difference(haystack, needles).length;
  }

  static relevantStyles(offset, styleRanges) {
    var styles = _.filter(styleRanges, function(range) {
      return (offset >= range.offset && offset < (range.offset + range.length));
    });
    return styles.map(function (style) {return style.style});
  }

  buildMarkup(rawDraftContentState, markup) {
    console.log(rawDraftContentState);
    var blocks = rawDraftContentState.blocks;
    return blocks.map(function convertBlock(block) {
      var outputText = [];
      var styleStack = [];
      var text = block.text;
      var ranges = block.inlineStyleRanges;
      var type = block.type;

      // loop over every char in this block's text
      for (var i = 0; i < text.length; i++) {

        // figure out what styles this char and the next char need
        // (regardless of whether there *is* a next char or not)
        var characterStyles = Home.relevantStyles(i, ranges);
        var nextCharacterStyles = Home.relevantStyles(i + 1, ranges);

        // calculate styles to add and remove
        var stylesToAdd = _.difference(characterStyles, styleStack);
        var stylesToRemove = _.difference(characterStyles, nextCharacterStyles);

        // add styles we will need for this char
        stylesToAdd.forEach(function(style) {
          styleStack.push(style);
          outputText.push(markup[style][0]);
        });

        outputText.push(text.substr(i, 1));

        // remove styles we won't need anymore
        while (Home.containsSome(styleStack, stylesToRemove)) {
          var toRemove = styleStack.pop();
          outputText.push(markup[toRemove][1]);
        }
      }
      return {
        blockType: type,
        styledMarkup: outputText.join('')
      }
    });

  }

  handleRender = (value) => {
    let markup = {
      'BOLD': ['<strong>', '</strong>'],
      'ITALIC': ['<em>', '</em>'],
      'LINK': ['<a href="#">','</a>'],
      'CODE': ['<code>','</code>']
    };
    var commentContent = (blocks) => {
      let commentText = "";
      blocks.forEach((block, index, arr) => {
        if(block.blockType == "unstyled") commentText += "<p>" + block.styledMarkup + "</p>";
        if(block.blockType == "blockquote") commentText += "<blockquote>" + block.styledMarkup + "</blockquote>";
        if(block.blockType == "code-block") commentText += "<div class='box'><pre style='white-space: pre-wrap;'><code>" + block.styledMarkup + "</code></pre></div>";
      });
      return commentText;
    }
    const state = this.buildMarkup(value, markup);
    this.setState({editorRender: commentContent(state)});
  };

  render() {
    const {request} = this.props;
    //console.log(this.state.editorRender);
    return (
      <div>
        <ScrollToTopOnMount/>
        <h1>Home page</h1>
        <ColorfulEditorExample onRender={this.handleRender} {...this.props}/>
        <br/>
        <br/>
        <br/>
        <br/>
        {this.state.editorRender}
      </div>
    );
  }
}
