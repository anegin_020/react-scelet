import { takeEvery, takeLatest } from 'redux-saga/effects';
import { call, put } from 'redux-saga/effects';
import {CHANGE_USERNAME} from './constants';

function* fetchUser(action) {
  console.log(`It's work`, action);
}

export function* homeInitSaga() {
  yield takeEvery(CHANGE_USERNAME, fetchUser);
}

export default [
  homeInitSaga,
];
