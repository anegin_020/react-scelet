import React, {Component, PureComponent} from 'react'
import ScrollToTopOnMount from '../../components/utils/ScrollToTopOnMount'
import {EditorState, SelectionState} from 'draft-js'
import RichEditor from '../../components/RichEditor/RichEditorComment';
import {addAnswerBlock, addBlockquoteComment, createDataForBlockquoteComment, addNewComponent} from '../../components/RichEditor/utils/helper'

export default class Home extends Component {

  constructor(props) {
    super(props);
    let ht1 = {
      "entityMap": {
        "0": {
          "type": "ANSWER_COMMENT",
          "mutability": "IMMUTABLE",
          "data": {
            "senderUrl": "/user/1",
            "senderThumb": "http://i866.photobucket.com/albums/ab230/nahank/tv20in20%20round%2013/70x70.jpg",
            "nickname": "Вася Пупкин NEW"
          }
        },
        "1": {
          "type": "BLOCKQUOTE_COMMENT",
          "mutability": "IMMUTABLE",
          "data": {
            "senderUrl": "#",
            "senderThumb": "http://i866.photobucket.com/albums/ab230/nahank/tv20in20%20round%2013/70x70.jpg",
            "urlComment": "/test/lol",
            "dateComment": "2017-03-15 00:00:00",
            "nickname": "Вася Пупкин",
            "quote": {
              "entityMap": {},
              "blocks": [{
                "key": "c4jl0",
                "text": "awdaw d",
                "type": "unstyled",
                "depth": 0,
                "inlineStyleRanges": [{"offset": 4, "length": 3, "style": "CODE"}],
                "entityRanges": [],
                "data": {}
              }, {
                "key": "8ab5n",
                "text": "awdawijdoawij aw]dawodij awodaowodjawidia",
                "type": "unstyled",
                "depth": 0,
                "inlineStyleRanges": [{"offset": 9, "length": 4, "style": "CODE"}, {
                  "offset": 21,
                  "length": 4,
                  "style": "CODE"
                }, {"offset": 34, "length": 3, "style": "CODE"}],
                "entityRanges": [],
                "data": {}
              }, {
                "key": "e7itt",
                "text": "aowjd apwdapw idjawopid awd",
                "type": "unstyled",
                "depth": 0,
                "inlineStyleRanges": [{"offset": 13, "length": 5, "style": "CODE"}, {
                  "offset": 24,
                  "length": 3,
                  "style": "CODE"
                }],
                "entityRanges": [],
                "data": {}
              }, {
                "key": "3qpvp",
                "text": "awdija pijdjawd",
                "type": "unstyled",
                "depth": 0,
                "inlineStyleRanges": [],
                "entityRanges": [],
                "data": {}
              }]
            }
          }
        }
      },
      "blocks": [{
        "key": "6jq6v",
        "text": " ",
        "type": "answer-comment",
        "depth": 0,
        "inlineStyleRanges": [],
        "entityRanges": [{"offset": 0, "length": 1, "key": 0}],
        "data": {}
      }, {
        "key": "8fep7",
        "text": "",
        "type": "unstyled",
        "depth": 0,
        "inlineStyleRanges": [],
        "entityRanges": [],
        "data": {}
      }, {
        "key": "djomb",
        "text": " ",
        "type": "blockquote-comment",
        "depth": 0,
        "inlineStyleRanges": [],
        "entityRanges": [{"offset": 0, "length": 1, "key": 1}],
        "data": {}
      }, {
        "key": "jnqo",
        "text": "awdaw dwad wad awd awd awddwa wad awdaw daw",
        "type": "unstyled",
        "depth": 0,
        "inlineStyleRanges": [],
        "entityRanges": [],
        "data": {}
      }, {
        "key": "cckr",
        "text": "awdawdaw awd awdawdaw dawdawd",
        "type": "unstyled",
        "depth": 0,
        "inlineStyleRanges": [],
        "entityRanges": [],
        "data": {}
      }, {
        "key": "d6ina",
        "text": "awd awdwdad",
        "type": "unstyled",
        "depth": 0,
        "inlineStyleRanges": [],
        "entityRanges": [],
        "data": {}
      }]
    }
    let ht = {"entityMap":{"0":{"type":"ANSWER_COMMENT","mutability":"IMMUTABLE","data":{"senderUrl":"http://netgamer.loc/article/official-singles-chart-top-100-1.html#c-18","senderThumb":"http://netgamer.loc/uploads/storage/avatar/thumbs/70x70\\MpADFL3RVzsQaldm.png","nickname":"Изя Кац (admin)"}}},"blocks":[{"key":"evrtd","text":"awdawdwadwad aw dawdawd","type":"unstyled","depth":0,"inlineStyleRanges":[],"entityRanges":[],"data":{}},{"key":"a7ouq","text":"awd aw awd awdaw awdaw awd","type":"unstyled","depth":0,"inlineStyleRanges":[{"offset":0,"length":6,"style":"BOLD"},{"offset":11,"length":5,"style":"BOLD"},{"offset":23,"length":3,"style":"ITALIC"}],"entityRanges":[],"data":{}},{"key":"c3mo0","text":" ","type":"answer-comment","depth":0,"inlineStyleRanges":[],"entityRanges":[{"offset":0,"length":1,"key":0}],"data":{}},{"key":"a86tu","text":"Пробная статья для нового редактора","type":"unstyled","depth":0,"inlineStyleRanges":[],"entityRanges":[],"data":{}},{"key":"91qad","text":"Жирность","type":"unstyled","depth":0,"inlineStyleRanges":[{"offset":0,"length":8,"style":"BOLD"}],"entityRanges":[],"data":{}},{"key":"32d6i","text":"Курсив","type":"unstyled","depth":0,"inlineStyleRanges":[{"offset":0,"length":6,"style":"ITALIC"}],"entityRanges":[],"data":{}},{"key":"erq7l","text":"вфцвфцвцф","type":"unordered-list-item","depth":0,"inlineStyleRanges":[{"offset":0,"length":9,"style":"ITALIC"}],"entityRanges":[],"data":{}},{"key":"dndv","text":"фцвфцвцфвц","type":"unordered-list-item","depth":0,"inlineStyleRanges":[{"offset":0,"length":10,"style":"ITALIC"}],"entityRanges":[],"data":{}},{"key":"a18gc","text":"фцвфцв","type":"unordered-list-item","depth":0,"inlineStyleRanges":[{"offset":0,"length":6,"style":"ITALIC"}],"entityRanges":[],"data":{}},{"key":"7sh81","text":"фцвцфвфцв","type":"ordered-list-item","depth":0,"inlineStyleRanges":[{"offset":0,"length":9,"style":"ITALIC"}],"entityRanges":[],"data":{}},{"key":"7a9td","text":"фцвфцвцфв","type":"ordered-list-item","depth":0,"inlineStyleRanges":[{"offset":0,"length":9,"style":"ITALIC"}],"entityRanges":[],"data":{}},{"key":"38m64","text":"фцвфцвцф","type":"ordered-list-item","depth":0,"inlineStyleRanges":[{"offset":0,"length":8,"style":"ITALIC"}],"entityRanges":[],"data":{}},{"key":"857jk","text":"фцвфцвцф вфцвфцфцв ","type":"blockquote","depth":0,"inlineStyleRanges":[{"offset":0,"length":19,"style":"ITALIC"},{"offset":0,"length":19,"style":"BOLD"}],"entityRanges":[],"data":{}},{"key":"acbf2","text":"фцвцфвфцвфц фцв","type":"header-one","depth":0,"inlineStyleRanges":[],"entityRanges":[],"data":{}}]}
    setTimeout(()=>{
      console.log('force');
      this.forceUpdate();
    }, 1000);
    this.state = {
      richTextState: undefined,
      richTextState2: RichEditor.createWithRowContent(ht),
      htmlRichTextState: '',
      renderText: '',
      selectText: '',
      refEditor: undefined,
      readOnly: false,
      renderState: true,
    };
  }

  handleChangeRichEditor = (EditorState) => {
    //console.dir(JSON.stringify(RichEditor.renderContentToRow(EditorState)))
    this.setState({
      richTextState: EditorState,
    })
  };

  handleChangeRichEditor2 = (EditorState) => {
    this.setState({
      richTextState2: EditorState,
    })
  };

  handleClick = () => {
    console.log(RichEditor.renderContentToHTML(this.state.richTextState))
    console.dir(JSON.stringify(RichEditor.renderContentToRow(this.state.richTextState)))
    this.setState({
      htmlRichTextState: RichEditor.renderContentToHTML(this.state.richTextState)
    })
    //this.setState({renderText: RichEditor.renderContent(this.state.richTextState.getCurrentContent())})
  };

  test(editorState) {
    let content = editorState.getCurrentContent()
    let blockMap = content.blockMap.filter((block) =>
      !(block.type == 'answer-comment' || block.type == 'blockquote-comment')
    )
    let selection = SelectionState.createEmpty(blockMap.first().getKey())
    let withoutAtomicBlock = content.merge({blockMap, selectionAfter: selection, selectionBefore: selection})
    return RichEditor.renderContentToRow(EditorState.push(editorState, withoutAtomicBlock, 'remove-range'))
  }

  alertObj(obj, _str) {
    let str = "{\r\n";
    for(let k in obj) {
      if(typeof obj[k] == 'object'){
        str += this.alertObj(obj[k], str);
      } else {
        str += k+": "+ obj[k]+"\r\n";
      }
    }
    str += "\r\n},"
    return str;
  }

  render() {
    const {request} = this.props;
    const {richTextState, renderText, richTextState2} = this.state;
    return (
      <div style={{display: 'flex'}}>
        <div style={{width: '60%'}}>
          <ScrollToTopOnMount/>
          <h1>Home page</h1>
          <br/>
          <RichEditor
            type="comment"
            onChange={this.handleChangeRichEditor}
            editorState={richTextState}
            readOnly={this.state.readOnly}
            ref={(refs) => {
              this.state.refEditor = refs
            }}
            onRemoveAnswerBlock={() => {
              console.log('onRemoveAnswerBlock')
            }}
            onSubmit={()=>{console.log('SUBMIT')}}
            theme={{
              bgColor: `#ffffff`
            }}
          />
          <br/>
          {/*<div>
           <RichEditor
           onChange={this.handleChangeRichEditor2}
           editorState={richTextState2}
           readOnly={true}
           theme={{
           bgColor: `#ffffff`
           }}
           />
           </div>*/}
          <br/>
          <button onClick={() => {
            var range = window.getSelection().getRangeAt(0),
              content = range.extractContents(),
              span = document.createElement('SPAN')
            console.log(range)
            span.appendChild(content);
            var htmlContent = span.innerHTML;

            range.insertNode(span)
            this.setState({selectText: htmlContent})
            console.log(htmlContent, {/*window.getSelection().selectAllChildren()*/})
          }}>GET QUOTE
          </button>
          <br/>
          <button onClick={() => {
            this.handleChangeRichEditor(addAnswerBlock(richTextState, {
              senderUrl: '/user/1',
              senderThumb: 'http://i866.photobucket.com/albums/ab230/nahank/tv20in20%20round%2013/70x70.jpg',
              nickname: 'Вася Пупкин NEW',
            }, this.state.refEditor))
          }}>ADD ANSWER
          </button>
          <br/>
          <button onClick={() => {
            this.handleChangeRichEditor(addBlockquoteComment(richTextState, {
              senderUrl: '#',
              senderThumb: 'http://i866.photobucket.com/albums/ab230/nahank/tv20in20%20round%2013/70x70.jpg',
              urlComment: '/test/lol',
              dateComment: '2017-03-15 00:00:00',
              nickname: 'Вася Пупкин',
              quote: createDataForBlockquoteComment(this.state.richTextState2)
            }, this.state.refEditor))
          }}>ADD BLOCKQUOTE
          </button>
          <br/>
          <button onClick={() => {
            this.handleChangeRichEditor(addNewComponent(richTextState, {
              src: 'https://images.pexels.com/photos/36764/marguerite-daisy-beautiful-beauty.jpg?w=1260&h=750&auto=compress&cs=tinysrgb',
              title: 'Ромашка',
            }, this.state.refEditor))
          }}>ADD IMAGE
          </button>
          <br/>
          <button onClick={() => {
            this.setState({readOnly: !this.state.readOnly})
          }}>READONLY
          </button>
          <br/>
          <h1 onClick={this.handleClick}>Click for render content</h1>
          <br/>
          {renderText}
          <br/>

          {<div dangerouslySetInnerHTML={{__html: this.state.htmlRichTextState}}/>}
        </div>
        <div style={{width: '40%', height: '100%', padding: '0 10px'}}>
          <pre style={{width: '100%', border: '2px solid gray', fontSize: '10px', maxHeight: '100vh', overflow: 'scroll' }}
               dangerouslySetInnerHTML={{__html: richTextState != undefined ? JSON.stringify(RichEditor.renderContentToRow(richTextState), null, "\t") : ''}}
          />
        </div>
      </div>
    );
  }
}
