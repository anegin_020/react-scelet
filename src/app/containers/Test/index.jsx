import React, { Component} from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Select from 'react-select';
import {Link} from 'react-router';
import Input from '../../components/elements/form/Input';
import Editor from '../../components/elements/form/Editor';
import LinkEditor from '../../components/elements/form/LinkEditor';
import 'react-select/dist/react-select.css';

var options = [
    { value: 'one', label: 'One' },
    { value: 'two', label: 'Two' }
];

class Test extends Component{

    constructor(props){
        super(props);
        this.state = {
            select: null,
        };
    }

    handleChange = (val) => {
        this.setState({
            select: val,
        });
    };

    render(){
        const {request} = this.props;
        console.log(this.state.select);
        return(
            <div>
                <h1>Test page</h1>
                {request && <strong>LOADING...</strong>}
                <Input defaultValue="Home page"/>
                <Editor />
                <br/>
                <LinkEditor />
                <Link to="/">Back to home</Link>
                <Select
                    name="form-field-name"
                    value={this.state.select}
                    options={options}
                    onChange={this.handleChange}
                />

            </div>
        );
    }
}

function mapStateToProps(state, ownProps) {
    return {
        id: ownProps.params.id,
        filter: ownProps.location.query.filter
    };
}

export default connect(mapStateToProps)(Test)