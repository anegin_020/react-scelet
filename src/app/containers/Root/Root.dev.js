import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Provider} from 'react-redux';
import {BrowserRouter as Router} from 'react-router-dom';
import LanguageProvider from '../LanguageProvider';
import DevTools from '../DevTools';

export default class Root extends Component {
  render() {
    const {store, history, createRoutes, messages} = this.props;

    return (
      <Provider store={store}>
        <div>
          <LanguageProvider messages={messages}>
            <Router history={history} children={createRoutes(store)} />
          </LanguageProvider>
          <DevTools />
        </div>
      </Provider>
    )
  }
}

Root.propTypes = {
  store: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
};
