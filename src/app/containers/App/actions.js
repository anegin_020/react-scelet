import {INITIALIZE} from './constants';

export const setInitialize = (state = false) => ({
    type: INITIALIZE,
    payload: state,
});