import { createSelector } from 'reselect';

const selectGlobal = state => state.get('global');

const makeInitialize = () => createSelector(
  selectGlobal,
  (globalState) => globalState.get('initialize')
);

const makeLocationState = () => {
  let prevRoutingState;
  let prevRoutingStateJS;

  return (state) => {
    const routingState = state.get('route');

    if (!routingState.equals(prevRoutingState)) {
      prevRoutingState = routingState;
      prevRoutingStateJS = routingState.toJS();
    }

    return prevRoutingStateJS;
  };
};

export {
  selectGlobal,
  makeInitialize,
  makeLocationState,
};
