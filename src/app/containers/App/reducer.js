import {fromJS} from 'immutable';
import {
    INITIALIZE,
    DEFAULT_LOCALE,
}  from './constants';

const initState = fromJS({
    initialize: false,
});

export default function(state = initState, action) {
    switch(action.type){
        case INITIALIZE:
            return state.set('initialize', action.payload);
        default:
            return state;
    }
}