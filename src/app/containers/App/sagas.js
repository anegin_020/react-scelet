import { takeEvery, takeLatest, fork } from 'redux-saga/effects';
import { call, put, all } from 'redux-saga/effects';
import SagasModel from '../../sagas/SagasModel';
import {APP_INITIALIZE} from '../../constants/AppConstants';

function* fetchUser(action) {
  console.log(`It's work`, action);
}

function* appInitSaga() {
  yield takeEvery(APP_INITIALIZE, fetchUser);
}

export default new SagasModel({
  init: function* (){
    yield all([
      fork(appInitSaga)
    ]);
  },
});
