import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import { createStructuredSelector } from 'reselect';
import {makeInitialize} from './selectors';
import {appInitialize} from '../../actions/AppActions.js';

class App extends Component {

  static propTypes = {
    initialize: PropTypes.bool,
    children: PropTypes.node,
    appInitialize: PropTypes.func,
  };

  componentWillMount() {
    this.props.appInitialize(true)
  }

  render() {
    const {children} = this.props;
    return (
      <section className="containers">
        {children}
      </section>
    )
  }
}

function mapStateToProps(state) {
  return createStructuredSelector({
    initialize: makeInitialize(state),
  });
}

export default connect(mapStateToProps, {
  appInitialize,
})(App)
