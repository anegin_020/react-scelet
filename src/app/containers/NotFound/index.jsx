import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

export default class NotFound extends PureComponent{

    render(){
        return(
            <h1>Sorry this page not found :(</h1>
        );
    }

}