import {createRequestTypes} from '../utils/AppUtils';

//export const GET_RANDOM_IMAGE = createRequestTypes('GET_RANDOM_IMAGE');

const constants = {
    GET_RANDOM_IMAGE: 'GET_RANDOM_IMAGE',
    GET_RANDOM_IMAGE_REQUEST: 'GET_RANDOM_IMAGE_REQUEST',
    GET_RANDOM_IMAGE_FAILURE: 'GET_RANDOM_IMAGE_FAILURE',
};

export default constants;