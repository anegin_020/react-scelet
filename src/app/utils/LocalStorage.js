export default class LocalStorage {

    static exsist(key) {
        return localStorage.getItem(key) !== null;
    }

    static set(key, value, timestamp = null) {
        if (timestamp == null) {
            let _timestampe = new Date();
            _timestampe.setHours(_timestampe.getHours() + 1);
            timestamp = _timestampe.valueOf();
        }
        localStorage.setItem(key, JSON.stringify({value, timestamp}));
        return LocalStorage.get(key);
    }

    static get(key) {
        let result = localStorage.getItem(key);
        if (result !== null) {
            result = JSON.parse(result);
            if (result.timestamp < new Date().valueOf()) {
                this.removeItem(key);
                return null;
            }
        } else return null;
        return result.value;
    }

    static removeItem(key) {
        return localStorage.removeItem(key);
    }

    static clear() {
        return localStorage.clear();
    }

}