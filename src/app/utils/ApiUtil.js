import LocalStorage from './LocalStorage';

function request(url, requestData, reconnect = 2) {
    return fetch(url, requestData)
        .then((res) => {
            if (statusSuccess(res.status)) {
                return res.json();
            }
            if (res.status == 404) {
                return Promise.reject(Promise.resolve({error: `Response with status ${res.status}`}));
            }
            if (res.status >= 500) {
                return !reconnect
                    ? Promise.reject(Promise.resolve({error: `Response with Server error (${res.status}).`}))
                    : request(url, requestData, --reconnect);
            }
            if (res.status >= 300 && res.status < 400) {
                return Promise.reject(Promise.resolve({error: `Response with redirect ${res.status}`}));
            }
            return Promise.reject(res.json());
        });
}

export function api(url, body = null, method = 'GET', form = false, rest = true) {
    let requestData = {
        method,
        credentials: rest ? 'same-origin' : 'include',
    };
    if (body) {
        requestData.body = !form ? JSON.stringify(body) : body;
    }
    if (!form) {
        requestData.headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        };
    }
    if(!url.match(/https?:\/\//)){
        url = url[0] != '/' ? `/${url}` : url;
    }
    return request(url, requestData);
}

export function statusSuccess(status) {
    return (status >= 200 && status < 300);
}

export function apiWithStorage(url, timestamp, ...argv) {
    var data = LocalStorage.get(url);
    if (data) {
        return Promise.resolve(data);
    }
    return api(url, ...argv)
        .then((json) => {
            return LocalStorage.set(url, json, timestamp);
        })
}