import {APP_INITIALIZE} from '../constants/AppConstants';

export const appInitialize = (state = false) => {
    return {
        type: APP_INITIALIZE,
        payload: state,
    }
}