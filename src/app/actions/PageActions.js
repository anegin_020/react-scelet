import {action} from '../utils/AppUtils';
import * as PageConstants from '../constants/PageConstants';

export const loadHomePage = () => action(PageConstants.LOAD_HOME_PAGE);