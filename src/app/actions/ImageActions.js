import ImageConstants from '../constants/ImageConstants';
import {action} from '../utils/AppUtils';

/*
 export const getRandomImage1 = {
 request: () => action(GET_RANDOM_IMAGE.REQUEST),
 success: (response) => action(GET_RANDOM_IMAGE.SUCCESS, {login, response}),
 failure: (error) => action(GET_RANDOM_IMAGE.FAILURE, {login, error}),
 };

 export const getRandomImage2 = ()=> action(GET_RANDOM_IMAGE);*/

export const loadImagesAction = () => {
    return {
        type: ImageConstants.GET_RANDOM_IMAGE_REQUEST,
        count: 5,
    }
};
