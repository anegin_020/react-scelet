import {Map} from 'immutable';
import ImageConstants  from '../constants/ImageConstants';

const initState = Map({
    request: false,
    apiKey: 'GZPyYxWWxYi7-BYChUJP',
    data: {},
    error: {},
});

export default function(state = initState, action) {
    switch(action.type){
        case ImageConstants.GET_RANDOM_IMAGE:
            return state.mergeDeep(Map({
                request: false,
                data: action.payload,
            }));
        case ImageConstants.GET_RANDOM_IMAGE_FAILURE:
            return state.mergeDeep(Map({
                request: false,
                data: {},
                error: action.error,
            }));
        case ImageConstants.GET_RANDOM_IMAGE_REQUEST:
            return state.update('request', value => true);
        default:
            return state;
    }
}