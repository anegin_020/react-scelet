import {combineReducers} from 'redux';
import { routerReducer as routing } from 'react-router-redux';
import App from "./AppReducer";
import Image from "./ImageReducer";

const rootReducer = combineReducers({
    routing,
    App,
    Image,
});

export default rootReducer;