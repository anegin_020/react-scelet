import {fromJS} from 'immutable';
import {APP_INITIALIZE}  from '../constants/AppConstants';
import {LOCATION_CHANGE} from 'react-router-redux';

const initState = fromJS({
    initialize: false,
    locationBeforeTransitions: {},
});

export default function(state = initState, action) {
    switch(action.type){
        case APP_INITIALIZE:
            return state.set('initialize', action.payload);
        case LOCATION_CHANGE:
            return state.set('locationBeforeTransitions', action.payload.pathname);
        default:
            return state;
    }
}