import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class Input extends Component{

    render(){
        const {
            type = 'text',
            defaultValue = '',
        } = this.props;
        return(
            <input type={type} defaultValue={defaultValue}/>
        );
    }

}

Input.propTypes = {
    type: PropTypes.string,
    defaultValue: PropTypes.string,
};

console.log('Create Input component');