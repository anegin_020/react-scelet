import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class Button extends Component{

    render(){
        return(
            <button id="supperBtn">
                {this.props.children}
            </button>
        );
    }

}

Button.propTypes = {
    children: React.PropTypes.oneOfType([
        React.PropTypes.arrayOf(React.PropTypes.node),
        React.PropTypes.node
    ])
};

console.log('Create Button compoent');