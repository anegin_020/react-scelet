import React, {PureComponent} from 'react'
import {ENTITY_TYPE} from '../constants/index'
import {Link} from 'react-router-dom'
import {EditorState, Modifier, SelectionState} from 'draft-js'

export class AnswerComment extends PureComponent{

  constructor(props){
    super(props)
    this.state = {
      select: false
    }
  }

  render(){
    const {senderUrl, senderThumb, urlComment, nickname} = this.props.contentState.getEntity(this.props.block.getEntityAt(0)).getData()
    const {editPosition} = this.props.blockProps
    let styles = {border: '1px solid', padding: '2px', userSelect: 'none'}
    if(this.state.select){
      styles.boxShadow = 'red 0 0 0 2px inset'
    }
    return (
      <div style={styles}
           onClick={()=>{
             if(this.state.select){
               editPosition(this.props.block.set('type', 'test-left'))
             } else {
               editPosition(this.props.block.set('type', 'test'))
             }
             this.setState({select: !this.state.select})
           }}
      >
        <Link to={senderUrl} ><img src={senderThumb} alt={nickname}/></Link>
        <Link to={urlComment} >{nickname}</Link>
        <span>awdawdaw kapwdakowmdoawdm aowdawdawdawdawm okmawokdmawkmd</span>
        <span>awdawdaw kapwdakowmdoawdm aowdawdawdawdawm okmawokdmawkmd</span>
      </div>
    )
  }

}

const findLinkEntities = (contentBlock, callback, contentState) => {
  contentBlock.findEntityRanges(
    (character) => {
      const entityKey = character.getEntity();
      return (
        entityKey !== null &&
        contentState.getEntity(entityKey).getType() === ENTITY_TYPE.ANSWER_COMMENT
      );
    },
    callback
  );
}

export default (props) => ({
  strategy: findLinkEntities,
  component: AnswerComment,
  ...props,
})
