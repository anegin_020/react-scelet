import React from 'react'
import {ENTITY_TYPE} from '../constants/index'
import {Link} from 'react-router-dom'

function RichLink(props) {
  const {url, reactive} = props.contentState.getEntity(props.entityKey).getData()
  if(reactive){
    return <Link to={url} >{props.children}</Link>
  } else {
    return <a href={url}>{props.children}</a>
  }
}

const findLinkEntities = (contentBlock, callback, contentState) => {
  contentBlock.findEntityRanges(
    (character) => {
      const entityKey = character.getEntity();
      if(entityKey !== null)
      return (
        entityKey !== null &&
        contentState.getEntity(entityKey).getType() === ENTITY_TYPE.LINK
      );
    },
    callback
  );
}

export default {
  strategy: findLinkEntities,
  component: RichLink,
}
