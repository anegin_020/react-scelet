import React from 'react'
import {ENTITY_TYPE} from '../constants/index'

function Image(props) {
  const {src, title} = props.contentState.getEntity(props.entityKey).getData()
  return <div>
    <div>
      <img src={src} alt={title}/>
    </div>
    <span>{title}</span>
  </div>
}

const findImageEntities = (contentBlock, callback, contentState) => {
  contentBlock.findEntityRanges(
    (character) => {
      const entityKey = character.getEntity();
      if(entityKey !== null)
      return (
        entityKey !== null &&
        contentState.getEntity(entityKey).getType() === ENTITY_TYPE.IMAGE
      );
    },
    callback
  );
}

export default {
  strategy: findImageEntities,
  component: Image,
}
