import React, {Component, PureComponent} from 'react'
import PropTypes from 'prop-types'
import {fromJS} from 'immutable'
import forEach from 'lodash/forEach'
import {
  EditorState,
  RichUtils,
  convertToRaw,
  convertFromHTML,
  convertFromRaw,
  ContentState,
  getDefaultKeyBinding,
  CompositeDecorator,
} from 'draft-js'

import LinkDecorator from './decorators/LinkDecorator'
const decorator = new CompositeDecorator([
  LinkDecorator,
  //AnswerCommentDecorator,
]);

export default class RichEditorWrapper extends Component {

  static propTypes = {
    readOnly: PropTypes.bool,
  }

  static defaultProps = {
    readOnly: false,
  }



  static createWithRowContent(rowContent) {
    forEach(rowContent.entityMap, function(value, key) {
      value.data.mention = fromJS(value.data.mention)
    })
    const contentState = convertFromRaw(rowContent)
    return EditorState.createWithContent(contentState, decorator)
  }

  static createWithHTMLContent(htmlContent) {
    const blocksFromHTML = convertFromHTML(htmlContent);
    const state = ContentState.createFromBlockArray(
      blocksFromHTML.contentBlocks,
      blocksFromHTML.entityMap,
    );

    return EditorState.createWithContent(state, decorator)
  }

  static createEmptyContent() {
    return EditorState.createEmpty(decorator);
  }

  static renderContent(currentState) {
    const rowState = convertToRaw(currentState);
    console.log(rowState);
    console.log(JSON.stringify(rowState));
    return 'Nothing';
  }

  customKeyHandler = (event) => {
    return getDefaultKeyBinding(event);
  }

  focus = () => {
    this.refs.editor.focus();
  }

  handleKeyCommand = (command) => {
    const {editorState, onChange} = this.props;
    const newState = RichUtils.handleKeyCommand(editorState, command);
    if (newState) {
      onChange(newState);
      return true;
    }
    return false;
  }
}

/*export default (WrappedComponent, ContentState, convertToRaw, convertFromHTML, convertFromRaw) => {

  return class extends Component{

    static createWithRowContent(rowContent) {
      forEach(rowContent.entityMap, function(value, key) {
        value.data.mention = fromJS(value.data.mention)
      })
      const contentState = convertFromRaw(rowContent)
      return EditorState.createWithContent(contentState, decorator)
    }

    static createWithHTMLContent(htmlContent) {
      const blocksFromHTML = convertFromHTML(htmlContent);
      const state = ContentState.createFromBlockArray(
        blocksFromHTML.contentBlocks,
        blocksFromHTML.entityMap,
      );

      return EditorState.createWithContent(state, decorator)
    }

    static createEmptyContent() {
      return EditorState.createEmpty(decorator);
    }

    static renderContent(currentState) {
      const rowState = convertToRaw(currentState);
      console.log(rowState);
      console.log(JSON.stringify(rowState));
      return 'Nothing';
    }

  }

}*/
