import {EditorState, Modifier, SelectionState} from 'draft-js';
import {OrderedSet, List, Repeat} from 'immutable';
import _extend from 'lodash/extend';

const limitSelection = (selection, block) => {
  return selection.set('anchorOffset', Math.max(selection.getStartOffset(), 0)).set('focusOffset', Math.min(block.getText().length, selection.getEndOffset()));
}

const createSelectionForChange = (change) => {
  return SelectionState.createEmpty(change.blockKey).set('anchorOffset', change.start).set('focusOffset', change.end);
}

const resetStateToUnstyled = (editorState) => {
  return EditorState.setInlineStyleOverride(editorState, []);
}

const applyInlineStyles = (content, selection, styles, incrementApplied) => {
  return styles.reduce((accContent, style) => {
    let block = content.getBlockForKey(selection.getStartKey())
    let chars = block.getCharacterList().slice(selection.getStartOffset(), selection.getEndOffset())
    let stylesInBlock = chars.map((e) => e.getStyle().toList())
    let styleList = (0, Repeat)((0, List)(styles), stylesInBlock.size)
    let needToChange = !stylesInBlock.equals(styleList)

    if (needToChange) {
      incrementApplied()
    }

    return Modifier.applyInlineStyle(accContent, selection, style)
  }, content)
}

const applyChangesToState = (changes, state) => {
  let changesApplied = 0;
  let incrementApplied = function incrementApplied() {
    return changesApplied += 1;
  };
  let content = changes.reduce(function (newContent, change) {
    return applyInlineStyles(newContent, limitSelection(createSelectionForChange(change), newContent.getBlockForKey(change.blockKey)), change.styles, incrementApplied);
  }, state.getCurrentContent());

  let maybeTheNewState = resetStateToUnstyled(EditorState.acceptSelection(EditorState.push(state, content, 'apply-inline-styles'), state.getSelection()));

  return !changesApplied ? state : maybeTheNewState;
}

const getChangesToApplyForState = (decorator, editorState) => {
  let changes = []
  let cb = (blockKey, start, end) => {
    return changes.push({ blockKey: blockKey, start: start, end: end, styles: decorator.styles })
  }
  let blockMap = editorState.getCurrentContent().getBlockMap()
  blockMap.forEach(function (block) {
    decorator.strategy(block, cb.bind(null, block.getKey()))
  })
  return changes
}

const syncExecuteStrategies = (decorators, ref) => {
  let getEditorState = ref.getEditorState
  let editorState = getEditorState()
  let changesToApply = decorators.reduce(function (changes, decorator) {
    let changesInDecorator = getChangesToApplyForState(decorator, editorState) || []
    return changes.concat(changesInDecorator);
  }, [])
  return applyChangesToState(changesToApply, getEditorState());
}

const executeStrategies = (decorators, callbacks) => {
  setTimeout(function () {
    callbacks.setEditorState(syncExecuteStrategies(decorators, callbacks) || callbacks.getEditorState())
  });
}


const stripStyles = (callbacks, afterwards) => {
  let state = callbacks.getEditorState();
  let currentContent = state.getCurrentContent();

  let newContent = currentContent.getBlockMap().reduce(function (contentState, block) {
    let allBlock = SelectionState.createEmpty(block.getKey()).set('anchorOffset', 0).set('focusOffset', block.getText().length);
    return Modifier.replaceText(contentState, allBlock, block.getText(), (0, OrderedSet)([]));
  }, currentContent);

  afterwards(EditorState.acceptSelection(EditorState.push(state, newContent), state.getSelection()));
}

const checkIfWeNeedToStripStyles = (callbacks, afterwards) => {
  let currentState = callbacks.getEditorState()
  let selectedState = EditorState.forceSelection(currentState, currentState.getSelection())

  if (selectedState.getCurrentInlineStyle().size > 0) {
    setTimeout(function () {
      return stripStyles(callbacks, afterwards)
    })
    return true
  }
}


export default (decorators) => ({
  onChange: (state) => {
    const callbacks = {
      getEditorState: () => state,
    };
    let newState = syncExecuteStrategies(decorators, callbacks) || state;
    return EditorState.setInlineStyleOverride(newState, (0, OrderedSet)([]))
  },
  handleKeyCommand: function handleKeyCommand(command, state, callbacks) {
    if (command === 'backspace') {
      checkIfWeNeedToStripStyles(callbacks, function (state) {
        executeStrategies(decorators, Object.assign({}, callbacks, {
          getEditorState: function getEditorState() {
            return state;
          }
        }));
      });
    }
  },
  handleReturn: function handleReturn(e, state, callbacks) {
    checkIfWeNeedToStripStyles(callbacks, function (newState) {
      executeStrategies(decorators, Object.assign({}, callbacks, {
        getEditorState: function getEditorState() {
          return newState;
        }
      }));
    });
  }
})
