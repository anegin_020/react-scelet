import BLOCK_TYPE from './BlockType'
import ENTITY_TYPE from './EntityType'
import INLINE_TYPE from './InlineType'
import OTHER_TYPE from './OtherType'

export {
  BLOCK_TYPE,
  ENTITY_TYPE,
  INLINE_TYPE,
  OTHER_TYPE,
}
