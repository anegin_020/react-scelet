const constants = {
  MAX_LIST_DEPTH: 2,
  TYPE_DEFAULT: 'default',
  TYPE_COMMENT: 'comment',
}

export default constants
