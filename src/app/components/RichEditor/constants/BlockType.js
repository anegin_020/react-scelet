const constants = {
  UNSTYLED: 'unstyled',
  HEADER_ONE: 'header-one',
  HEADER_TWO: 'header-two',
  HEADER_THREE: 'header-three',
  HEADER_FOUR: 'header-four',
  HEADER_FIVE: 'header-five',
  HEADER_SIX: 'header-six',
  UNORDERED_LIST_ITEM: 'unordered-list-item',
  ORDERED_LIST_ITEM: 'ordered-list-item',
  BLOCKQUOTE: 'blockquote',
  BLOCKQUOTE_COMMENT: 'blockquote-comment',
  ANSWER_COMMENT: 'answer-comment',
  PULLQUOTE: 'pullquote',
  CODE_BLOCK: 'code-block',
  ATOMIC: 'atomic',
  IMAGE: 'image',
}

export default constants
