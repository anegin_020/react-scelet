import React, {Component, PureComponent} from 'react'
import PropTypes from 'prop-types'
import {fromJS} from 'immutable'
import forEach from 'lodash/forEach'
import {
  Editor,
  EditorState,
  Modifier,
  RichUtils,
  CompositeDecorator,
  convertToRaw,
  convertFromHTML,
  convertFromRaw,
  ContentState,
  getDefaultKeyBinding,
  KeyBindingUtil
} from 'draft-js'
import EditorToolBar from './utils/ToolBar'
import LinkDecorator from './decorators/LinkDecorator'
import AnswerCommentDecorator from './decorators/AnswerCommentDecorator'
import RichEditorWrapper from './RichEditorWrapper'
import {AnswerComment} from './decorators/AnswerCommentDecorator'

const styles = {
  hashtag: {
    color: 'rgb(0, 129, 255)',
    direction: 'ltr',
    unicodeBidi: 'bidi-override',
    fontSize: '0.9em',
  },
  handle: {
    color: 'rgb(0, 86, 78)',
    direction: 'ltr',
    unicodeBidi: 'bidi-override',
    fontSize: '0.9em',
  },
  body: {
    background: 'transparent',
    fontFamily: '"Lato", serif',
    fontSize: '1em',
    padding: '0 20px',
  },
  editor: {
    background: '#fff',
    border: '1px solid #ccc',
    padding: '20px 10px',
    cursor: 'text',
    fontSize: '1em',
    margin: '10px 0',
    fontFamily: "Lato",
    transition: 'all 0.2s ease-in',
  },
  editorRead: {
    background: 'transparent',
  }
}

const decorator = new CompositeDecorator([
  LinkDecorator,
  //AnswerCommentDecorator,
]);

const styleMap = {
  CODE: {
    display: 'inline-block',
    backgroundColor: 'rgba(0, 0, 0, 0.05)',
    fontFamily: '"Inconsolata", "Menlo", "Consolas", monospace',
    fontSize: 16,
    padding: 2,
    marginTop: 3
  },
}

export default class RichEditor extends RichEditorWrapper {

  _blockRenderer = (block) => {
    if (block.getType() === 'atomic') {
      console.log(block.toJS());
      return {
        component: AnswerComment,
        editable: false,
      };
    }
    return null;
  };



  render() {
    const {editorState, onChange, readOnly} = this.props;
    let editorToolbar = null;
    if(readOnly === false){
      editorToolbar = <EditorToolBar
        editorState={editorState}
        onChange={onChange}
        focusEditor={this.focus}
      />
    }
    return <div style={styles.body}>
      {editorToolbar}
      <div
        style={readOnly ? styles.editorRead : styles.editor}
        onClick={this.focus}
      >
        <Editor
          blockRendererFn={this._blockRenderer}
          editorState={editorState}
          keyBindingFn={this.customKeyHandler}
          handleKeyCommand={this.handleKeyCommand}
          customStyleMap={styleMap}
          onChange={onChange}
          placeholder="Be cool."
          ref="editor"
          spellCheck={true}
          readOnly={readOnly}
        />
      </div>
    </div>
  }
}
