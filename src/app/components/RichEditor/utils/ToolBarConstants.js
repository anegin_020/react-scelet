import {ENTITY_TYPE, BLOCK_TYPE, INLINE_TYPE} from '../constants'

export const INLINE_STYLE_BUTTONS = [
  {label: 'Bold', style: INLINE_TYPE.BOLD},
  {label: 'Italic', style: INLINE_TYPE.ITALIC},
  {label: 'Strikethrough', style: INLINE_TYPE.STRIKETHROUGH},
  {label: 'Monospace', style: INLINE_TYPE.CODE},
  {label: 'Underline', style: INLINE_TYPE.UNDERLINE},
];

export const BLOCK_TYPE_DROPDOWN = [
  {label: 'Normal', style: BLOCK_TYPE.UNSTYLED},
  {label: 'Heading Large', style: BLOCK_TYPE.HEADER_ONE},
  // {label: 'Heading Medium', style: BLOCK_TYPE.HEADER_TWO},
  // {label: 'Heading Small', style: BLOCK_TYPE.HEADER_THREE},
  {label: 'Code Block', style: BLOCK_TYPE.CODE_BLOCK},
];

export const BLOCK_TYPE_BUTTONS = [
  {label: 'UL', style: BLOCK_TYPE.UNORDERED_LIST_ITEM},
  {label: 'OL', style: BLOCK_TYPE.ORDERED_LIST_ITEM},
  {label: 'Blockquote', style: BLOCK_TYPE.BLOCKQUOTE},
];

export const LINK_BUTTONS = {
  LINK_ADD: {label: 'Добавить ссылку', style: ENTITY_TYPE.LINK},
  LINK_REMOVE: {label: 'Удалить ссылку', style: ENTITY_TYPE.LINK},
}

export default {
  display: ['INLINE_STYLE_BUTTONS', 'BLOCK_TYPE_BUTTONS', 'LINK_BUTTONS', 'BLOCK_TYPE_DROPDOWN', 'HISTORY_BUTTONS'],
  constants: {
    INLINE_STYLE_BUTTONS: 'INLINE_STYLE_BUTTONS',
    BLOCK_TYPE_BUTTONS: 'BLOCK_TYPE_BUTTONS',
    BLOCK_TYPE_DROPDOWN: 'BLOCK_TYPE_DROPDOWN',
    LINK_BUTTONS: 'LINK_BUTTONS',
    HISTORY_BUTTONS: 'HISTORY_BUTTONS',
  },
  INLINE_STYLE_BUTTONS,
  BLOCK_TYPE_DROPDOWN,
  BLOCK_TYPE_BUTTONS,
  LINK_BUTTONS,
};
