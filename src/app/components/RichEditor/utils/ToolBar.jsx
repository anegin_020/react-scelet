import React, {PureComponent} from 'react'
import PropTypes from 'prop-types'
import {hasCommandModifier} from 'draft-js/lib/KeyBindingUtil'
import {
  EditorState,
  RichUtils,
} from 'draft-js'
import {BLOCK_TYPE, ENTITY_TYPE} from '../constants'
import getEntityAtCursor from './getEntityAtCursor'
import clearEntityForRange from './clearEntityForRange'
import ToolBarConfig from './ToolBarConstants'
import ButtonGroup from '../components/ButtonGroup'
import StyleButton from '../components/StyleButton'
import IconButton from '../components/IconButton'
import PopapLink from '../components/PopapLink'
import DropdownBlock from '../components/DropdownBlock'
import {checkLinkReact, insertBlock} from './helper'
import '../styles/toolBar.css'

export default class ToolBar extends PureComponent {

  static propTypes = {
    editorState: PropTypes.instanceOf(EditorState).isRequired,
    onChange: PropTypes.func.isRequired,
    focusEditor: PropTypes.func.isRequired,
    onAddAnswerBlock: PropTypes.func,
    onAddAnswerBlockquote: PropTypes.func,
  };

  constructor(props) {
    super(props)
    this.state = {
      showLinkInput: false,
    }
  }

  componentWillMount() {
    window.addEventListener('keypress', this, false)
  }

  componentWillUnmount() {
    window.removeEventListener('keypress', this, false)
  }

  handleEvent(e) {
    switch (e.type) {
      case 'keypress':
        this.handleKeypress(e)
        break
    }
  }

  handleKeypress = (event) => {
    if (hasCommandModifier(event) && event.keyCode === 75) {
      let {editorState} = this.props
      if (!editorState.getSelection().isCollapsed()) {
        this.setState({showLinkInput: true})
      }
    }
  }

  handleEditShowLinkInput = () => {
    this.setState((state) => ({...state, showLinkInput: !state.showLinkInput}))
  }

  handleSetLink = (url) => {
    let {editorState} = this.props;
    const contentState = editorState.getCurrentContent();
    const contentStateWithEntity = contentState.createEntity(
      ENTITY_TYPE.LINK,
      'MUTABLE',
      {
        url: url,
        reactive: checkLinkReact(url),
      }
    );
    const entityKey = contentStateWithEntity.getLastCreatedEntityKey();
    const newEditorState = EditorState.set(editorState, {currentContent: contentStateWithEntity});
    this.props.onChange(
      RichUtils.toggleLink(
        newEditorState,
        newEditorState.getSelection(),
        entityKey
      )
    );

    this.focusEditor();
    this.setState((state) => ({...state, showLinkInput: false}))
  }

  handleCancelLink = () => {
    this.setState((state) => ({...state, showLinkInput: false}))
  }

  focusEditor = () => {
    setTimeout(() => {
      this.props.focusEditor();
    }, 50)
  }

  toggleInlineStyle = (inlineStyle) => {
    this.props.onChange(
      RichUtils.toggleInlineStyle(
        this.props.editorState,
        inlineStyle
      )
    )
  }

    toggleBlockType = (blockType) => {
    this.props.onChange(
      RichUtils.toggleBlockType(
        this.props.editorState,
        blockType
      )
    )
  }

  removeLink = () => {
    let {editorState, onChange} = this.props
    let entity = getEntityAtCursor(editorState)
    if (entity != null) {
      let {blockKey, startOffset, endOffset} = entity;
      onChange(
        clearEntityForRange(editorState, blockKey, startOffset, endOffset)
      )
    }
  }

  selectBlockType = (blockType) => {
    this.toggleBlockType(blockType)
    this.focusEditor()
  }

  getEntity = () => {
    let entity = getEntityAtCursor(this.props.editorState)
    let contentState = this.props.editorState.getCurrentContent()
    return (entity == null) ? null : contentState.getEntity(entity.entityKey)
  }

  getCurrentBlockType = () => {
    let {editorState} = this.props
    let selection = editorState.getSelection()
    return editorState
      .getCurrentContent()
      .getBlockForKey(selection.getStartKey())
      .getType()
  }

  renderInlineStyleButtons = (groupName, toolBarConfig) => {
    let {editorState} = this.props
    let currentStyle = editorState.getCurrentInlineStyle()
    let buttons = (toolBarConfig[groupName] || []).map((type, index) => (
      <StyleButton
        key={index}
        isActive={currentStyle.has(type.style)}
        label={type.label}
        style={type.style}
        onToggle={this.toggleInlineStyle}
      />
    ))
    return (
      <ButtonGroup key={groupName}>{buttons}</ButtonGroup>
    )
  }

  renderLinkButtons = (groupName, toolBarConfig) => {
    let {editorState} = this.props
    let selection = editorState.getSelection()
    let entity = this.getEntity()
    let hasSelection = !selection.isCollapsed()
    let isCursorOnLink = (entity != null && entity.type === ENTITY_TYPE.LINK)
    let shouldShowLinkButton = hasSelection || isCursorOnLink
    return (
      <ButtonGroup key={groupName}>
        <IconButton
          title={toolBarConfig.LINK_BUTTONS.LINK_ADD.label}
          text={toolBarConfig.LINK_BUTTONS.LINK_ADD.label}
          icon={ENTITY_TYPE.LINK_ADD}
          isDisabled={!shouldShowLinkButton}
          onClick={this.handleEditShowLinkInput}
          wrapper={this.state.showLinkInput ? <PopapLink
            onSubmit={this.handleSetLink}
            onCancel={this.handleCancelLink}
          /> : undefined}
        />
        <IconButton
          title={toolBarConfig.LINK_BUTTONS.LINK_REMOVE.label}
          text={toolBarConfig.LINK_BUTTONS.LINK_REMOVE.label}
          icon={ENTITY_TYPE.LINK_REMOVE}
          isDisabled={!isCursorOnLink}
          onClick={this.removeLink}
        />
      </ButtonGroup>
    );
  }

  renderDropdownBlockType = (groupName, toolBarConfig) => {
    let blockType = this.getCurrentBlockType()
    const options = toolBarConfig[groupName] || []
    if (options.find((option) => option.style === blockType) == undefined) {
      blockType = options[0] ? options[0].style : null
    }
    return (
      <ButtonGroup key={groupName}>
        <DropdownBlock
          options={options}
          blockType={blockType}
          onChange={this.selectBlockType}
        />
      </ButtonGroup>
    )
  }

  renderBlockType = (groupName, toolBarConfig) => {
    let blockType = this.getCurrentBlockType()
    const buttons = (toolBarConfig[groupName] || []).map((value, key) => (
      <StyleButton
        key={key}
        isActive={value.style === blockType}
        label={value.label}
        style={value.style}
        onToggle={this.toggleBlockType}
      />
    ))
    return (
      <ButtonGroup key={groupName}>
        {buttons}
      </ButtonGroup>
    )
  }

  render() {
    let buttonsGroup = ToolBarConfig.display.map((groupName) => {
      switch (groupName) {
        case ToolBarConfig.constants.INLINE_STYLE_BUTTONS:
          return this.renderInlineStyleButtons(groupName, ToolBarConfig)
        case ToolBarConfig.constants.LINK_BUTTONS:
          return this.renderLinkButtons(groupName, ToolBarConfig)
        case ToolBarConfig.constants.BLOCK_TYPE_DROPDOWN:
          return this.renderDropdownBlockType(groupName, ToolBarConfig)
        case ToolBarConfig.constants.BLOCK_TYPE_BUTTONS:
          return this.renderBlockType(groupName, ToolBarConfig)
      }
    })
    return <div className="rt_toolbar">
      {buttonsGroup}
    </div>
  }

}
