import React, {PureComponent} from 'react'
import PropTypes from 'prop-types'
import {DefaultDraftBlockRenderMap} from 'draft-js'
import {Map} from 'immutable'
import '../styles/wrapperBlock.css'
import {BLOCK_TYPE, OTHER_TYPE} from '../constants'
export const divWrapper = <div className="div-wrapper"/>
export const divLeftWrapper = <div className="div-wrapper-left"/>
export const codeBlockWrapper = <pre className="code_block-wrapper" />
export const figureWrapper = <figure className="test figure"/>
export const figureLeftWrapper = <figure className="test figure" style={{float: 'left'}}/>
export const blockquoteWrapper = <blockquote className="rt_blockquote" />
export const imageWrapper = <div className="rt_image"/>

const extendedCommentBlockRenderMap = (type) => DefaultDraftBlockRenderMap.merge(Map(Object.assign({}, {
  'atomic': {
    wrapper: divWrapper,
  },
  'test': {
    wrapper: figureWrapper,
  },
  'test-left': {
    wrapper: figureLeftWrapper,
  },
  [BLOCK_TYPE.BLOCKQUOTE_COMMENT]: {
    wrapper: divWrapper,
  },
  [BLOCK_TYPE.ANSWER_COMMENT]: {
    wrapper: divLeftWrapper,
  },
  'blockquote': {
    wrapper: blockquoteWrapper,
  },
  'code-block': {
    element: 'pre',
    wrapper: codeBlockWrapper,
  },
  'image': {
    wrapper: imageWrapper,
    aliasedElements: ['img']
  }
}, type == OTHER_TYPE.TYPE_COMMENT
  ? {
    'header-one': {
      element: 'b',
      aliasedElements: ['h1']
    },
    'header-two': {
      element: 'b',
      aliasedElements: ['h2']
    },
    'header-three': {
      element: 'b',
      aliasedElements: ['h3']
    },
    'header-four': {
      element: 'b',
      aliasedElements: ['h4']
    },
    'header-five': {
      element: 'b',
      aliasedElements: ['h5']
    },
    'header-six': {
      element: 'b',
      aliasedElements: ['h6']
    },
  }
  : {}
)))

export {
  extendedCommentBlockRenderMap
}
