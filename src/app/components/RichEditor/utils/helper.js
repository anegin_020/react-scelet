import React from 'react'
import {fromJS, List, Repeat} from 'immutable'
import {blockquoteWrapper, imageWrapper} from './blockRenderMap'
import {BLOCK_TYPE, ENTITY_TYPE, INLINE_TYPE} from '../constants'
import {
  EditorState,
  convertFromRaw,
  convertFromHTML,
  convertToRaw,
  ContentState,
  CompositeDecorator,
  Modifier,
  genKey,
  CharacterMetadata,
  ContentBlock,
  SelectionState,
  RichUtils
} from 'draft-js'
import {convertToHTML,} from 'draft-convert'
import BlockMapBuilder from 'draft-js/lib/BlockMapBuilder'
import LinkDecorator from '../decorators/LinkDecorator'
import ImageDecorator from '../decorators/ImageDecorator'
import {cbStringStyle} from './StyleMap'
import {ImageComponent} from '../components/renderFn/ImageComponent';

const decorators = [
  LinkDecorator,
  ImageDecorator,
]

export const checkLinkReact = (url) => (
  !!url.match(new RegExp(`(^\/\/|^\/|https?://${window.location.hostname})`))
)

export const helperCreateWithRowContent = (rowContent, newDecorators) => {
  /*forEach(rowContent.entityMap, function(value, key) {
   value.data.mention = fromJS(value.data.mention)
   })*/
  const contentState = convertFromRaw(rowContent)
  if (newDecorators == undefined) {
    return EditorState.createWithContent(contentState, new CompositeDecorator(decorators))
  }
  return EditorState.createWithContent(contentState, new CompositeDecorator(decorators.concat(newDecorators)))
}

export const helperCreateWithHTMLContent = (htmlContent, newDecorators) => {
  if (!htmlContent) {
    return helperCreateEmptyContent(newDecorators);
  }
  const blocksFromHTML = convertFromHTML(htmlContent);
  const state = ContentState.createFromBlockArray(
    blocksFromHTML.contentBlocks,
    blocksFromHTML.entityMap,
  );
  if (newDecorators == undefined) {
    return EditorState.createWithContent(state, new CompositeDecorator(decorators))
  }
  return EditorState.createWithContent(state, new CompositeDecorator(decorators.concat(newDecorators)))
}

export const helperCreateWithContent = (state, newDecorators) => {
  if (newDecorators == undefined) {
    return EditorState.createWithContent(state, new CompositeDecorator(decorators))
  }
  return EditorState.createEmpty(state, new CompositeDecorator(decorators.concat(newDecorators)))
}

export const helperCreateEmptyContent = (newDecorators) => {
  if (newDecorators == undefined) {
    return EditorState.createEmpty(new CompositeDecorator(decorators))
  }
  return EditorState.createEmpty(new CompositeDecorator(decorators.concat(newDecorators)))
}

function blockToHTML(block) {
  if (block.type === BLOCK_TYPE.ANSWER_COMMENT) {
    return <strong style={{float: 'left', marginRight: '5px'}}/>;
  }
  if (block.type === BLOCK_TYPE.BLOCKQUOTE) {
    return blockquoteWrapper
  }
  if (block.type === BLOCK_TYPE.IMAGE) {
    return imageWrapper
  }
  if (block.type === BLOCK_TYPE.BLOCKQUOTE_COMMENT) {
    return ''
  }
  if (block.type === BLOCK_TYPE.CODE_BLOCK) {
    return <pre className="code_block-wrapper"/>
  }
}

function entityToHTML(entity, originalText) {
  if (entity.type === ENTITY_TYPE.ANSWER_COMMENT) {
    return `${entity.data.nickname}, `
  }
  if (entity.type === ENTITY_TYPE.LINK) {
    return
  }
  if (entity.type === ENTITY_TYPE.IMAGE) {
    return <ImageComponent src={entity.data.src} title={entity.data.title}/>
  }
  return originalText;
}

export const helperRenderContentToRow = (currentState) => {
  return convertToRaw(currentState)
}

export const helperRenderContentToHTML = (currentState) => {
  return convertToHTML({
    blockToHTML: blockToHTML,
    entityToHTML: entityToHTML,
    styleToHTML: cbStringStyle,
  })(currentState)
}

export const insertBlock = (editorState, entityKey, character, blockType = 'atomic') => {
  let contentState = editorState.getCurrentContent()
  let selectionState = editorState.getSelection()

  let afterRemoval = Modifier.removeRange(contentState, selectionState, 'backward')

  let targetSelection = afterRemoval.getSelectionAfter()
  let afterSplit = Modifier.splitBlock(afterRemoval, targetSelection)
  let insertionTarget = afterSplit.getSelectionAfter()

  let asAtomicBlock = Modifier.setBlockType(afterSplit, insertionTarget, blockType)

  let charData = CharacterMetadata.create({entity: entityKey})
  let fragmentArray = [new ContentBlock({
    key: genKey(),
    type: blockType,
    text: character,
    characterList: List(Repeat(charData, character.length))
  }), new ContentBlock({
    key: genKey(),
    type: 'unstyled',
    text: '',
    characterList: List()
  })]

  let fragment = BlockMapBuilder.createFromArray(fragmentArray)

  let withAtomicBlock = Modifier.replaceWithFragment(asAtomicBlock, insertionTarget, fragment)

  let newContent = withAtomicBlock.merge({
    selectionBefore: selectionState,
    selectionAfter: withAtomicBlock.getSelectionAfter().set('hasFocus', true)
  })
  return EditorState.push(editorState, newContent, 'insert-fragment')
}

export function insertBlockAfter(editorState, blockKey, newType) {
  let content = editorState.getCurrentContent();
  let blockMap = content.getBlockMap();
  let block = blockMap.get(blockKey);
  let blocksBefore = blockMap.toSeq().takeUntil((v) => (v === block));
  let blocksAfter = blockMap.toSeq().skipUntil((v) => (v === block)).rest();
  let newBlockKey = genKey();
  let newBlock = new ContentBlock({
    key: newBlockKey,
    type: newType,
    text: '',
    characterList: block.getCharacterList().slice(0, 0),
    depth: 0,
  });
  let newBlockMap = blocksBefore.concat(
    [[blockKey, block], [newBlockKey, newBlock]],
    blocksAfter,
  ).toOrderedMap();
  let selection = editorState.getSelection();
  let newContent = content.merge({
    blockMap: newBlockMap,
    selectionBefore: selection,
    selectionAfter: selection.merge({
      anchorKey: newBlockKey,
      anchorOffset: 0,
      focusKey: newBlockKey,
      focusOffset: 0,
      isBackward: false,
    }),
  });
  return EditorState.push(editorState, newContent, 'split-block');
}

export const addAnswerBlock = (editorState, data, element) => {
  if (data && data.senderUrl && data.senderThumb && data.nickname) {
    const contentState = editorState.getCurrentContent();
    const contentStateWithEntity = contentState.createEntity(
      ENTITY_TYPE.ANSWER_COMMENT,
      'IMMUTABLE',
      data,
    )
    const entityKey = contentStateWithEntity.getLastCreatedEntityKey();
    const newEditorState = EditorState.set(
      editorState,
      {currentContent: contentStateWithEntity},
    )

    const newContentState = newEditorState.getCurrentContent()
    let charData = CharacterMetadata.create({entity: entityKey})
    let answerBlocks = [new ContentBlock({
      key: genKey(),
      type: BLOCK_TYPE.ANSWER_COMMENT,
      text: ' ',
      characterList: List(Repeat(charData, 1))
    })]

    const oldBlocks = newContentState.getBlockMap().filter((block) => block.getType() !== BLOCK_TYPE.ANSWER_COMMENT).toArray()
    const firstBlockType = oldBlocks[0].getType()
    if (firstBlockType === BLOCK_TYPE.ORDERED_LIST_ITEM || firstBlockType === BLOCK_TYPE.UNORDERED_LIST_ITEM || firstBlockType === BLOCK_TYPE.BLOCKQUOTE) {
      answerBlocks = answerBlocks.concat(new ContentBlock({
        key: genKey(),
        type: 'unstyled',
        text: '',
        characterList: List()
      }))
    }
    const newBlocks = answerBlocks.concat(oldBlocks)
    let contentStateWithSelect = ContentState.createFromBlockArray(newBlocks).merge({
      selectionBefore: SelectionState.createEmpty(oldBlocks[0].getKey()),
      selectionAfter: SelectionState.createEmpty(oldBlocks[0].getKey()).set('hasFocus', true)
    })
    if (element != undefined) {
      scrollToElement(element)
    }
    return EditorState.push(newEditorState, contentStateWithSelect)
  }
}

export const addBlockquoteComment = (editorState, data, element) => {
  const contentState = editorState.getCurrentContent();
  const contentStateWithEntity = contentState.createEntity(
    ENTITY_TYPE.BLOCKQUOTE_COMMENT,
    'IMMUTABLE',
    data,
  )
  const entityKey = contentStateWithEntity.getLastCreatedEntityKey();
  const newEditorState = EditorState.set(
    editorState,
    {currentContent: contentStateWithEntity},
  )
  if (element != undefined) {
    scrollToElement(element)
  }
  return insertBlock(newEditorState, entityKey, ' ', BLOCK_TYPE.BLOCKQUOTE_COMMENT)
}

export const addNewComponent = (editorState, data, element) => {
  const contentState = editorState.getCurrentContent();
  const contentStateWithEntity = contentState.createEntity(
    ENTITY_TYPE.IMAGE,
    'IMMUTABLE',
    data,
  )
  const entityKey = contentStateWithEntity.getLastCreatedEntityKey();
  const newEditorState = EditorState.set(
    editorState,
    {currentContent: contentStateWithEntity},
  )
  if (element != undefined) {
    scrollToElement(element)
  }
  return insertBlock(newEditorState, entityKey, ' ', BLOCK_TYPE.IMAGE)
}

export const onPasteQuote = (editorState, html) => {
  html = html[1] + html[3]
  let htmlWithoutNextLine = html.match(/(.*)(<br class="Apple-interchange-newline">)/m)
  if (htmlWithoutNextLine !== null) {
    html = htmlWithoutNextLine[1]
  }

  const quoteEditorState = helperCreateWithHTMLContent(html)
  const quoteBlockMap = quoteEditorState.getCurrentContent().getBlockMap()
  const quoteEntityMap = quoteEditorState.getCurrentContent().getEntityMap()
  let newContent
  let newEntityMap
  if (quoteBlockMap.size != 1) {

    let selection = new SelectionState({
      anchorKey: quoteBlockMap.first().getKey(),
      anchorOffset: 0,
      focusKey: quoteBlockMap.last().getKey(),
      focusOffset: (quoteBlockMap.last().getText().length),
      hasFocus: false,
    })
    const contentWithBlockQuote = Modifier.setBlockType(quoteEditorState.getCurrentContent(), selection, BLOCK_TYPE.BLOCKQUOTE)
    let fragment = contentWithBlockQuote.getBlockMap()
    newEntityMap = contentWithBlockQuote.getEntityMap()
    newContent = Modifier.replaceWithFragment(editorState.getCurrentContent(), editorState.getSelection(), fragment)

  } else {

    let fragment = quoteBlockMap
    newEntityMap = quoteEntityMap
    newContent = Modifier.replaceWithFragment(editorState.getCurrentContent(), editorState.getSelection(), fragment)
    newContent = Modifier.setBlockType(newContent, newContent.getSelectionAfter(), BLOCK_TYPE.BLOCKQUOTE)

  }

  return EditorState.push(editorState, newContent, 'insert-fragment')
}

function scrollToElement(theElement) {
  var selectedPosY, scrollRange = undefined, limitAttempts = 9
  let _selectedPosX = 0
  let _selectedPosY = 0
  theElement.focus()
  theElement = theElement.refs['editor'].refs['editor']
  while (theElement != null) {
    _selectedPosX += theElement.offsetLeft
    _selectedPosY += theElement.offsetTop
    theElement = theElement.offsetParent
  }
  selectedPosY = _selectedPosY - 70
  requestAnimationFrame(function scrollToEditorAnimation() {
    --limitAttempts
    if (scrollRange == undefined) {
      scrollRange = Math.round((Math.abs(selectedPosY - window.pageYOffset) / 10) * 100) / 100
      if (selectedPosY < window.pageYOffset) {
        scrollRange = -scrollRange
      }
    }
    window.scrollBy(0, scrollRange)
    if (!((window.pageYOffset - 50) < selectedPosY && (window.pageYOffset + 50) > selectedPosY) && limitAttempts > 0) {
      requestAnimationFrame(scrollToEditorAnimation)
    }
  })
}

export const createDataForBlockquoteComment = (editorState) => {
  let content = editorState.getCurrentContent()
  let blockMap = content.blockMap.filter((block) =>
    !(block.type == 'answer-comment' || block.type == 'blockquote-comment')
  )
  let selection = SelectionState.createEmpty(blockMap.first().getKey())
  let withoutAtomicBlock = content.merge({blockMap, selectionAfter: selection, selectionBefore: selection})
  return helperRenderContentToRow(EditorState.push(editorState, withoutAtomicBlock, 'remove-range').getCurrentContent())
}

export function isListItem(block) {
  let blockType = block.getType();
  return (
    blockType === BLOCK_TYPE.UNORDERED_LIST_ITEM ||
    blockType === BLOCK_TYPE.ORDERED_LIST_ITEM
  );
}

export function changeBlockType(editorState, blockKey, newType) {
  let content = editorState.getCurrentContent();
  let block = content.getBlockForKey(blockKey);
  let type = block.getType();
  if (type === newType) {
    return editorState;
  }
  let newBlock = block.set('type', newType);
  let newContent = content.merge({
    blockMap: content.getBlockMap().set(blockKey, newBlock),
  });
  return EditorState.push(
    editorState,
    newContent,
    'change-block-type'
  );
}

export function changeBlockDepth(editorState, blockKey, newDepth) {
  let content = editorState.getCurrentContent();
  let block = content.getBlockForKey(blockKey);
  let depth = block.getDepth();
  if (depth === newDepth) {
    return editorState;
  }
  let newBlock = block.set('depth', newDepth);
  let newContent = content.merge({
    blockMap: content.getBlockMap().set(blockKey, newBlock)
  });
  return EditorState.push(editorState, newContent, 'adjust-depth');
}

export const getIconClassByStyle = (style) => {
  switch (style) {
    case INLINE_TYPE.BOLD:
      return 'NIcon-bold'
    case INLINE_TYPE.CODE:
      return 'NIcon-embed'
    case INLINE_TYPE.ITALIC:
      return 'NIcon-italic'
    case INLINE_TYPE.STRIKETHROUGH:
      return 'NIcon-strikethrough'
    case INLINE_TYPE.UNDERLINE:
      return 'NIcon-underline'
    case BLOCK_TYPE.UNORDERED_LIST_ITEM:
      return 'NIcon-list2'
    case BLOCK_TYPE.ORDERED_LIST_ITEM:
      return 'NIcon-list-numbered'
    case BLOCK_TYPE.BLOCKQUOTE:
      return 'NIcon-quote-comment'
    case ENTITY_TYPE.LINK_ADD:
      return 'NIcon-link'
    case ENTITY_TYPE.LINK_REMOVE:
      return 'NIcon-link-remove'
  }
}
