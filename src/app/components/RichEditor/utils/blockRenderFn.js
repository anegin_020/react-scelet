import React from 'react'
import {AnswerComment as _AnswerComment} from '../decorators/AnswerCommentDecorator'
import AnswerComment from '../components/renderFn/AnswerComment'
import BlockquoteComment from '../components/renderFn/BlockquoteComment'
import ImageComponentHOC from '../components/renderFn/ImageComponent'

const atomicRenderFn = (props = {}, editable = false) => ({
  component: _AnswerComment,
  editable,
  props,
})

const testRenderFn = (props = {}, editable = false) => ({
  component: _AnswerComment,
  editable,
  props,
})

const commentQuoteFn = (props = {}, editable = false) => ({
  component: BlockquoteComment,
  editable,
  props,
})

const commentAnswerFn = (props = {}, editable = false) => ({
  component: AnswerComment,
  editable,
  props,
})

const imageRenderFn = (props = {}, editable = false) => ({
  component: ImageComponentHOC,
  editable,
  props,
})

export {
  atomicRenderFn,
  testRenderFn,
  commentQuoteFn,
  commentAnswerFn,
  imageRenderFn,
}
