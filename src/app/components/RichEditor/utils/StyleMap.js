export const objectStyle = {
  CODE: {
    backgroundColor: '#f3f3f3',
    fontFamily: '"Inconsolata", "Menlo", "Consolas", monospace',
    fontSize: 16,
    padding: 2,
  },
}

export const cbStringStyle = (style) => {
  switch (style){
    case 'STRIKETHROUGH':
      return {
        start: '<span style="text-decoration: line-through">',
        end: '</span>'
      };
    case 'CODE':
      return {
        start: '<code style="background-color: #f3f3f3; font-family: \'Inconsolata\', \'Menlo\', \'Consolas\', monospace; font-size: 16px; padding: 2px">',
        end: '</code>',
      }
  }
}
