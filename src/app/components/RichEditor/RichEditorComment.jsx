import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {Map} from 'immutable'
import {
  Editor,
  EditorState,
  RichUtils,
  getDefaultKeyBinding,
  KeyBindingUtil,
  Modifier,
} from 'draft-js'
import RichTextEditorUtil from 'draft-js/lib/RichTextEditorUtil'
import isSoftNewlineEvent from 'draft-js/lib/isSoftNewlineEvent';
import EditorToolBar from './utils/ToolBar'
import {
  helperCreateWithRowContent,
  helperCreateWithHTMLContent,
  helperCreateEmptyContent,
  helperRenderContentToRow,
  helperRenderContentToHTML,
  helperCreateWithContent,
  onPasteQuote,
  isListItem,
  changeBlockDepth,
  changeBlockType,
  insertBlockAfter,
} from './utils/helper'
import {
  testRenderFn,
  commentQuoteFn,
  commentAnswerFn,
  imageRenderFn,
} from './utils/blockRenderFn'
import {ENTITY_TYPE, OTHER_TYPE, BLOCK_TYPE} from './constants'
import {extendedCommentBlockRenderMap} from './utils/blockRenderMap'
import {objectStyle} from './utils/StyleMap'
import 'draft-js/dist/Draft.css'
import './styles/richText.css'

export default class RichEditor extends Component {

  static propTypes = {
    name: PropTypes.string,
    hardControl: PropTypes.bool,
    editorState: PropTypes.any,
    placeholder: PropTypes.string,
    onRemoveAnswerBlock: PropTypes.func,
    quotesMap: PropTypes.instanceOf(Map),
    className: PropTypes.string,
    onChange: PropTypes.func,
    onSubmit: PropTypes.func,
    readOnly: PropTypes.bool,
    inQuote: PropTypes.bool,
    theme: PropTypes.shape({
      bgColor: PropTypes.string,
    }),
    type: PropTypes.oneOf([OTHER_TYPE.TYPE_DEFAULT, OTHER_TYPE.TYPE_COMMENT]),
  }

  static defaultProps = {
    name: 'Editor',
    editorState: helperCreateEmptyContent(),
    hardControl: false,
    readOnly: false,
    inQuote: false,
    placeholder: 'Начните вводить текст...'
  }

  componentWillReceiveProps(nextProps) {
    /* if (this.props.readOnly !== nextProps.readOnly) {
     this.forceUpdate()
     }*/
  }

  handleChange = (value) => {
    if(!this.props.readOnly){
      this.props.onChange(value)
    }
  }

  handleReturn = (event) => {
    const {hasCommandModifier} = KeyBindingUtil;
    if(event.keyCode === 13 && hasCommandModifier(event) && this.props.onSubmit){
      this.props.onSubmit(this.props.editorState);
      return true;
    }
    if (this.handleReturnSoftNewline(event)) {
      return true;
    }
    if (this.handleReturnEmptyListItem()) {
      return true;
    }
    if (this.handleReturnSpecialBlock()) {
      return true;
    }
    return false;
  }

  // `shift + return` should insert a soft newline.
  handleReturnSoftNewline(event){
    const {editorState} = this.props;
    if (isSoftNewlineEvent(event)) {
      let selection = editorState.getSelection();
      if (selection.isCollapsed()) {
        this.props.onChange(RichUtils.insertSoftNewline(editorState));
      } else {
        let content = editorState.getCurrentContent();
        let newContent = Modifier.removeRange(content, selection, 'forward');
        let newSelection = newContent.getSelectionAfter();
        let block = newContent.getBlockForKey(newSelection.getStartKey());
        newContent = Modifier.insertText(
          newContent,
          newSelection,
          '\n',
          block.getInlineStyleAt(newSelection.getStartOffset()),
          null,
        );
        this.props.onChange(
          EditorState.push(editorState, newContent, 'insert-fragment')
        );
      }
      return true;
    }
    return false;
  }

  // If the cursor is in an empty list item when return is pressed, then the
  // block type should change to normal (end the list).
  handleReturnEmptyListItem(){
    const {editorState} = this.props;
    let selection = editorState.getSelection();
    if (selection.isCollapsed()) {
      let contentState = editorState.getCurrentContent();
      let blockKey = selection.getStartKey();
      let block = contentState.getBlockForKey(blockKey);
      if (isListItem(block) && block.getLength() === 0) {
        let depth = block.getDepth();
        let newState = (depth === 0) ?
          changeBlockType(editorState, blockKey, BLOCK_TYPE.UNSTYLED) :
          changeBlockDepth(editorState, blockKey, depth - 1);
        this.props.onChange(newState);
        return true;
      }
    }
    return false;
  }

  // If the cursor is at the end of a special block (any block type other than
  // normal or list item) when return is pressed, new block should be normal.
  handleReturnSpecialBlock(){
    const {editorState} = this.props;
    let selection = editorState.getSelection();
    if (selection.isCollapsed()) {
      let contentState = editorState.getCurrentContent();
      let blockKey = selection.getStartKey();
      let block = contentState.getBlockForKey(blockKey);
      if (!isListItem(block) && block.getType() !== BLOCK_TYPE.UNSTYLED) {
        if (block.getLength() === selection.getStartOffset()) {
          let newEditorState = insertBlockAfter(
            editorState,
            blockKey,
            BLOCK_TYPE.UNSTYLED
          );
          this.props.onChange(newEditorState);
          return true;
        }
      }
    }
    return false;
  }

  getReadOnly = () => this.props.readOnly

  deleteBlockByKey = (key) => {
    const {editorState} = this.props
    let content = editorState.getCurrentContent()
    const blockMap = content.getBlockMap().delete(key)
    let withoutAtomicBlock = content.merge({blockMap})
    if (withoutAtomicBlock !== content) {
      this.props.onChange(EditorState.push(editorState, withoutAtomicBlock, 'remove-range'))
    }
  }

  deleteAnswer = () => {
    console.log('DELETE ANSWER')
  }

  blockRenderer = (block) => {
    console.log(block.toJS());
    if (block.getType() === 'atomic') {
      return commentAnswerFn({
        deleteAnswer: this.deleteAnswer,
      })
    }
    if (block.getType() === BLOCK_TYPE.IMAGE) {
      return imageRenderFn({
        getReadOnly: this.getReadOnly,
      })
    }
    if (block.getType() === 'test' || block.getType() === 'test-left') {
      return testRenderFn({
        editPosition: this.editPosition
      })
    }
    if (block.getType() === BLOCK_TYPE.BLOCKQUOTE_COMMENT) {
      return commentQuoteFn({
        theme: this.props.theme,
        getReadOnly: this.getReadOnly,
        deleteBlockByKey: this.deleteBlockByKey,
      })
    }
    if (block.getType() === BLOCK_TYPE.ANSWER_COMMENT) {
      return commentAnswerFn({
        deleteAnswer: this.deleteAnswer,
        getReadOnly: this.getReadOnly,
      })
    }
    return null;
  };

  static createWithRowContent(rowContent) {
    return helperCreateWithRowContent(rowContent)
  }

  static createWithHTMLContent(htmlContent) {
    return helperCreateWithHTMLContent(htmlContent)
  }

  static createWithContent(htmlContent) {
    return helperCreateWithContent(htmlContent)
  }

  static createEmptyContent() {
    return helperCreateEmptyContent();
  }

  static renderContentToRow(editorState) {
    if(!(editorState instanceof EditorState)) return undefined
    return helperRenderContentToRow(editorState.getCurrentContent())
  }

  static renderContentToHTML(editorState) {
    if(!(editorState instanceof EditorState)) return undefined
    return helperRenderContentToHTML(editorState.getCurrentContent())
  }

  static removeEmptyLine(editorState) {
    const contentState = editorState.getCurrentContent();
    const entityMap = contentState.getEntity(1)
    contentState.getBlockMap().find(b => {

      return b.getEntityAt(0) === ENTITY_TYPE.ANSWER_COMMENT
    })
    const newAtomicBlock = contentState.getBlockMap().find(b => b.getEntityAt(0) === ENTITY_TYPE.ANSWER_COMMENT).getKey();
    const newBlockMap = contentState.getBlockMap().delete(contentState.getBlockBefore(newAtomicBlock)).delete(contentState.getBlockAfter(newAtomicBlock));
    const newContentState = contentState.set('blockMap', newBlockMap);
    return EditorState.push(editorState, newContentState, 'delete-empty-lines');
  }

  editPosition = (newBlock) => {
    const {editorState} = this.props
    let currentState = editorState.getCurrentContent()
    let newContent = currentState.set('blockMap', currentState.blockMap.set(newBlock.get('key'), newBlock))
    this.props.onChange(EditorState.push(editorState, newContent, 'change-block-type'))
  }

  customKeyHandler = (event) => {

    /*if ( {
      console.log('SUBMIT');
      return 'submit';
    }*/
    return getDefaultKeyBinding(event)
  }

  focus = () => {
    this.refs.editor.focus()
  }

  forceUpdate = () => {
    let {editorState} = this.props
    let content = editorState.getCurrentContent()
    this.props.onChange(RichEditor.createWithContent(content))
  }

  onDelete = (editorState) => {
    let selection = editorState.getSelection()
    if (!selection.isCollapsed() || selection.getAnchorOffset() || selection.getFocusOffset()) {
      return null
    }

    let content = editorState.getCurrentContent()
    let startKey = selection.getStartKey()
    let blockAfter = content.getBlockAfter(startKey)

    const blockAfterType = blockAfter ? blockAfter.getType() : null
    if (blockAfterType && (blockAfterType === 'atomic' || blockAfterType === BLOCK_TYPE.BLOCKQUOTE_COMMENT || blockAfterType === BLOCK_TYPE.ANSWER_COMMENT || blockAfterType === BLOCK_TYPE.IMAGE)) {
      const blockMap = content.getBlockMap().delete(blockAfter.getKey())
      let withoutAtomicBlock = content.merge({blockMap, selectionAfter: selection})
      if (withoutAtomicBlock !== content) {
        if (blockAfterType === BLOCK_TYPE.ANSWER_COMMENT && this.props.onRemoveAnswerBlock) {
          setTimeout(() => {
            this.props.onRemoveAnswerBlock()
          }, 1)
        }
        return EditorState.push(editorState, withoutAtomicBlock, 'remove-range')
      }
    }

    let withoutBlockStyle = RichTextEditorUtil.tryToRemoveBlockStyle(editorState)

    if (withoutBlockStyle) {
      return EditorState.push(editorState, withoutBlockStyle, 'change-block-type')
    }

    return null
  }

  onBackspace = (editorState) => {
    let selection = editorState.getSelection()
    if (!selection.isCollapsed() || selection.getAnchorOffset() || selection.getFocusOffset()) {
      return null
    }

    let content = editorState.getCurrentContent()
    let startKey = selection.getStartKey()
    let blockBefore = content.getBlockBefore(startKey)

    const blockBeforeType = blockBefore ? blockBefore.getType() : null
    if (blockBeforeType && (blockBeforeType === 'atomic' || blockBeforeType === BLOCK_TYPE.BLOCKQUOTE_COMMENT || blockBeforeType === BLOCK_TYPE.ANSWER_COMMENT || blockBeforeType === BLOCK_TYPE.IMAGE)) {
      const blockMap = content.getBlockMap().delete(blockBefore.getKey())
      let withoutAtomicBlock = content.merge({blockMap, selectionAfter: selection})
      if (withoutAtomicBlock !== content) {
        if (blockBeforeType === BLOCK_TYPE.ANSWER_COMMENT && this.props.onRemoveAnswerBlock) {
          setTimeout(() => {
            this.props.onRemoveAnswerBlock()
          }, 1)
        }
        return EditorState.push(editorState, withoutAtomicBlock, 'remove-range')
      }
    }

    let withoutBlockStyle = RichTextEditorUtil.tryToRemoveBlockStyle(editorState)

    if (withoutBlockStyle) {
      return EditorState.push(editorState, withoutBlockStyle, 'change-block-type')
    }

    return null
  }

  shouldHidePlaceholder = () => {
    let {editorState} = this.props
    let contentState = editorState.getCurrentContent();
    if (!contentState.hasText()) {
      if (contentState.getBlockMap().first().getType() !== 'unstyled') {
        return true;
      }
    }
    return false;
  }

  handleKeyCommand = (command) => {
    const {editorState} = this.props
    let newState = undefined
    switch (command) {
      case 'delete':
        newState = this.onDelete(editorState)
        break;
      case 'backspace':
      case 'backspace-word':
      case 'backspace-to-start-of-line':
        newState = this.onBackspace(editorState)
        break;
    }
    if (newState === undefined) {
      newState = RichUtils.handleKeyCommand(editorState, command)
    }
    if (newState) {
      this.props.onChange(newState)
      return true
    }
    return false
  }

  handleTab = (event) => {
    const {editorState} = this.props
    let newEditorState = RichUtils.onTab(event, editorState, OTHER_TYPE.MAX_LIST_DEPTH)
    if (newEditorState !== editorState) {
      this.props.onChange(newEditorState)
    }
  }

  render() {
    const {readOnly, type, className, placeholder, name} = this.props
    const {editorState} = this.props
    let editorToolbar = null
    if (readOnly === false) {
      editorToolbar = <EditorToolBar
        editorState={editorState}
        onChange={this.handleChange}
        focusEditor={this.focus}
      />
    }
    return <div className={`rt_body${className ? ` ${className}` : ``}`}>
      {editorToolbar}
      <div
        className={`${readOnly ? "rt_editor_read" : "rt_editor"}${this.shouldHidePlaceholder() ? ' rt_hide_placeholder' : ''}`}
        onClick={this.focus}
        id={name}
      >
        <Editor
          blockRenderMap={extendedCommentBlockRenderMap(type)}
          blockRendererFn={this.blockRenderer}
          editorState={editorState}
          //keyBindingFn={this.customKeyHandler}
          handleKeyCommand={this.handleKeyCommand}
          customStyleMap={objectStyle}
          onTab={this.handleTab}
          handleReturn={this.handleReturn}
          handlePastedText={(text, html) => {
            if (html != undefined) {
              let htmlFragment = html.match(/\<body.*?\<\/body\>|\<\!--StartFragment-->.*?\<\!--EndFragment--\>/mi);
              if (htmlFragment == null){
                return false;
              }
              let result = htmlFragment[0].match(/(.*)(<span id="ctrlcopy".+?><\/span>)(.*)/mi);
              const {editorState} = this.props;
              if (result !== null) {
                this.props.onChange(onPasteQuote(editorState, result))
                return true
              }
            }
          }}
          placeholder={placeholder}
          ref="editor"
          spellCheck={true}
          readOnly={readOnly}
          onChange={this.handleChange}
        />
      </div>
    </div>
  }
}
