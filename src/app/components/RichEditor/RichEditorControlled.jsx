import React, {Component} from 'react'
import PropTypes from 'prop-types'
import {Map} from 'immutable'
import {
  Editor,
  EditorState,
  RichUtils,
  getDefaultKeyBinding,
  KeyBindingUtil,
} from 'draft-js'
import RichTextEditorUtil from 'draft-js/lib/RichTextEditorUtil'
import EditorToolBar from './utils/ToolBar'
import {
  helperCreateWithRowContent,
  helperCreateWithHTMLContent,
  helperCreateEmptyContent,
  helperRenderContentToRow,
  helperRenderContentToHTML,
  helperCreateWithContent,
  onPasteQuote,
} from './utils/helper'
import {
  testRenderFn,
  commentQuoteFn,
  commentAnswerFn,
} from './utils/blockRenderFn'
import {ENTITY_TYPE, OTHER_TYPE, BLOCK_TYPE} from './constants'
import {extendedCommentBlockRenderMap} from './utils/blockRenderMap'
import 'draft-js/dist/Draft.css'
import './styles/richText.css'

const styleMap = {
  CODE: {
    backgroundColor: '#f3f3f3',
    fontFamily: '"Inconsolata", "Menlo", "Consolas", monospace',
    fontSize: 16,
    padding: 2,
  },
}

export default class RichEditor extends Component {

  static propTypes = {
    hardControl: PropTypes.bool,
    editorState: PropTypes.any,
    placeholder: PropTypes.string,
    onRemoveAnswerBlock: PropTypes.func,
    quotesMap: PropTypes.instanceOf(Map),
    className: PropTypes.string,
    onChange: PropTypes.func,
    onSubmit: PropTypes.func,
    readOnly: PropTypes.bool,
    inQuote: PropTypes.bool,
    theme: PropTypes.shape({
      bgColor: PropTypes.string,
    })
  }

  static defaultProps = {
    editorState: helperCreateEmptyContent(),
    hardControl: false,
    readOnly: false,
    inQuote: false,
    placeholder: 'Начните вводить текст...'
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.readOnly !== nextProps.readOnly) {
      this.forceUpdate()
    }
  }

  handleChange = (value) => {
    this.props.onChange(value)
  }

  getReadOnly = () => this.props.readOnly

  deleteBlockByKey = (key) => {
    const {editorState} = this.props
    let content = editorState.getCurrentContent()
    const blockMap = content.getBlockMap().delete(key)
    let withoutAtomicBlock = content.merge({blockMap})
    if (withoutAtomicBlock !== content) {
      this.props.onChange(EditorState.push(editorState, withoutAtomicBlock, 'remove-range'))
    }
  }

  deleteAnswer = () => {
    console.log('DELETE ANSWER')
  }

  blockRenderer = (block) => {
    if (block.getType() === 'atomic') {
      return commentAnswerFn({
        deleteAnswer: this.deleteAnswer,
      })
    }
    if (block.getType() === 'test' || block.getType() === 'test-left') {
      return testRenderFn({
        editPosition: this.editPosition
      })
    }
    if (block.getType() === BLOCK_TYPE.BLOCKQUOTE_COMMENT) {
      return commentQuoteFn({
        theme: this.props.theme,
        getReadOnly: this.getReadOnly,
        deleteBlockByKey: this.deleteBlockByKey,
      })
    }
    if (block.getType() === BLOCK_TYPE.ANSWER_COMMENT) {
      return commentAnswerFn({
        deleteAnswer: this.deleteAnswer,
        getReadOnly: this.getReadOnly,
      })
    }
    return null;
  };

  static createWithRowContent(rowContent) {
    return helperCreateWithRowContent(rowContent)
  }

  static createWithHTMLContent(htmlContent) {
    return helperCreateWithHTMLContent(htmlContent)
  }

  static createWithContent(htmlContent) {
    return helperCreateWithContent(htmlContent)
  }

  static createEmptyContent() {
    return helperCreateEmptyContent();
  }

  static renderContentToRow(editorState) {
    return helperRenderContentToRow(editorState.getCurrentContent())
  }

  static renderContentToHTML(editorState) {
    return helperRenderContentToHTML(editorState.getCurrentContent())
  }

  static removeEmptyLine(editorState) {
    const contentState = editorState.getCurrentContent();
    const entityMap = contentState.getEntity(1)
    console.log(entityMap)
    contentState.getBlockMap().find(b => {
      console.log(b.toJS())
      return b.getEntityAt(0) === ENTITY_TYPE.ANSWER_COMMENT
    })
    const newAtomicBlock = contentState.getBlockMap().find(b => b.getEntityAt(0) === ENTITY_TYPE.ANSWER_COMMENT).getKey();
    const newBlockMap = contentState.getBlockMap().delete(contentState.getBlockBefore(newAtomicBlock)).delete(contentState.getBlockAfter(newAtomicBlock));
    const newContentState = contentState.set('blockMap', newBlockMap);
    return EditorState.push(editorState, newContentState, 'delete-empty-lines');
  }

  editPosition = (newBlock) => {
    const {editorState} = this.props
    let currentState = editorState.getCurrentContent()
    let newContent = currentState.set('blockMap', currentState.blockMap.set(newBlock.get('key'), newBlock))
    this.props.onChange(EditorState.push(editorState, newContent, 'change-block-type'))
  }

  customKeyHandler = (event) => {
    const {hasCommandModifier} = KeyBindingUtil;
    if (event.keyCode === 13 && hasCommandModifier(event)) {
      return 'submit';
    }
    return getDefaultKeyBinding(event)
  }

  focus = () => {
    this.refs.editor.focus()
  }

  forceUpdate = () => {
    let {editorState} = this.props
    let content = editorState.getCurrentContent()
    this.props.onChange(RichEditor.createWithContent(content))
  }

  onBackspace = (editorState) => {
    let selection = editorState.getSelection()
    if (!selection.isCollapsed() || selection.getAnchorOffset() || selection.getFocusOffset()) {
      return null
    }

    let content = editorState.getCurrentContent()
    let startKey = selection.getStartKey()
    let blockBefore = content.getBlockBefore(startKey)

    const blockBeforeType = blockBefore ? blockBefore.getType() : null
    if (blockBeforeType && (blockBeforeType === 'atomic' || blockBeforeType === BLOCK_TYPE.BLOCKQUOTE_COMMENT || blockBeforeType === BLOCK_TYPE.ANSWER_COMMENT)) {
      const blockMap = content.getBlockMap().delete(blockBefore.getKey())
      let withoutAtomicBlock = content.merge({blockMap, selectionAfter: selection})
      if (withoutAtomicBlock !== content) {
        if (blockBeforeType === BLOCK_TYPE.ANSWER_COMMENT && this.props.onRemoveAnswerBlock) {
          setTimeout(() => {
            this.props.onRemoveAnswerBlock()
          }, 1)
        }
        return EditorState.push(editorState, withoutAtomicBlock, 'remove-range')
      }
    }

    let withoutBlockStyle = RichTextEditorUtil.tryToRemoveBlockStyle(editorState)

    if (withoutBlockStyle) {
      return EditorState.push(editorState, withoutBlockStyle, 'change-block-type')
    }

    return null
  }

  handleKeyCommand = (command) => {
    const {editorState} = this.props
    let newState = undefined
    switch (command) {
      case 'backspace':
      case 'backspace-word':
      case 'backspace-to-start-of-line':
        newState = this.onBackspace(editorState)
        break;
      case 'submit':
        if(this.props.onSubmit){
          this.props.onSubmit(editorState);
          return false;
        }
        break;
    }
    if (newState === undefined) {
      newState = RichUtils.handleKeyCommand(editorState, command)
    }
    if (newState) {
      this.props.onChange(newState)
      return true
    }
    return false
  }

  handleTab = (event) => {
    const {editorState} = this.props
    let newEditorState = RichUtils.onTab(event, editorState, OTHER_TYPE.MAX_LIST_DEPTH)
    if (newEditorState !== editorState) {
      this.props.onChange(newEditorState)
    }
  }

  render() {
    const {readOnly, inQuote, theme, className, placeholder} = this.props
    const {editorState} = this.props
    let editorToolbar = null
    if (readOnly === false) {
      editorToolbar = <EditorToolBar
        editorState={editorState}
        onChange={this.handleChange}
        focusEditor={this.focus}
      />
    }
    return <div className={`rt_body${className ? ` ${className}` : ``}`}>
      {editorToolbar}
      <div
        className={readOnly ? "rt_editor_read" : "rt_editor"}
        onClick={this.focus}
      >
        <Editor
          blockRenderMap={extendedCommentBlockRenderMap(inQuote, theme)}
          blockRendererFn={this.blockRenderer}
          editorState={editorState}
          keyBindingFn={this.customKeyHandler}
          handleKeyCommand={this.handleKeyCommand}
          customStyleMap={styleMap}
          onTab={this.handleTab}
          handlePastedText={(text, html) => {
            if (html != undefined) {
              html = html.match(/\<body.*?\<\/body\>|\<\!--StartFragment-->.*?\<\!--EndFragment--\>/mi)[0];
              let result = html.match(/(.*)(<span id="ctrlcopy".+?><\/span>)(.*)/mi);
              const {editorState} = this.props;
              if (result !== null) {
                this.props.onChange(onPasteQuote(editorState, result))
                return true
              }
            }
          }}
          placeholder={placeholder}
          ref="editor"
          spellCheck={true}
          readOnly={readOnly}
          onChange={this.handleChange}
        />
      </div>
    </div>
  }
}
