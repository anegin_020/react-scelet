import React from 'react';
import '../styles/buttonGroup.css'

export default (props) => (
  <div {...props} className="rt_button_group" />
)
