import React, {PureComponent} from 'react'
import '../styles/wrapperDropdown.css'

export default class DropdownBlock extends PureComponent {

  handleChange = (event) => {
    this.props.onChange(event.target.value)
  }

  renderOptions = () => (this.props.options.map((option, key) =>
    <option value={option.style} key={key}>{option.label}</option>
  ))

  render() {
    const {options, blockType} = this.props
    const selectedValue = options.find(option => option.style === blockType) || {}
    return (
      <span className="rt_dropdown_select">
        <select name="dropdownBlock" id="dropdownBlock" value={blockType} onChange={this.handleChange}>
          {this.renderOptions()}
        </select>
        <span>{selectedValue.label}</span>
      </span>
    )
  }

}
