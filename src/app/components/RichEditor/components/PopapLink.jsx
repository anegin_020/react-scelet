import React, {PureComponent} from 'react'
import PropTypes from 'prop-types'
import IconButton from './IconButton'
import '../styles/wrapperLink.css'

export default class PopapLink extends PureComponent {

  static propTypes = {
    onSubmit: PropTypes.func.isRequired,
    onCancel: PropTypes.func.isRequired,
  }

  constructor(props){
    super(props)
    this.state = {fixPosition: 0}
  }

  componentDidMount() {
    document.addEventListener('keydown', this, true)
    document.addEventListener('touchstart', this, true)
    document.addEventListener('mousedown', this, true)
    const clientReact = this.refs['popap_link'].getBoundingClientRect()
    if(clientReact.right > (window.innerWidth - 40)){
      this.setState({fixPosition: window.innerWidth - clientReact.right  - 40})
    }
  }

  componentWillUnmount() {
    document.removeEventListener('keydown', this, true)
    document.removeEventListener('touchstart', this, true)
    document.removeEventListener('mousedown', this, true)
  }

  handleEvent(e) {
    switch (e.type) {
      case 'keydown':
        ::this.closeByKey(e);
        break;
      case 'mousedown':
        ::this.closeByClick(e);
        break;
      case 'touchstart':
        ::this.closeByClick(e);
        break;
    }
  }

  closeByKey = (e) => {
    if (e.keyCode == 27) {
      this.props.onCancel()
    }
  }

  closeByClick(e) {
    let obj = e.target
    let flag = false
    while (!flag && obj.tagName != 'BODY') {
      if (obj == this.refs['popap_link']) {
        flag = true
      }
      obj = obj.parentNode;
      if (obj == null) {
        flag = true
      }
    }
    if (!flag) {
      this.props.onCancel()
    }
  }

  handleClickSuccess = () => {
    this.props.onSubmit(this.refs['rich_editor_link'].value)
  }

  handleFormSubmit = (event) => {
    this.props.onSubmit(this.refs['rich_editor_link'].value)
  }

  handleClickCancel = () => {
    this.props.onCancel()
  }

  render() {
    return <div className="rt_popapLink" ref="popap_link" style={{left: `${this.state.fixPosition}px`}}>
      <form ref="popap_link-form" onSubmit={this.handleFormSubmit}>
        <input type="text" name="rich_editor_link" ref="rich_editor_link"/>
        <IconButton
          text="Подтвердить"
          title="Подтвердить"
          onClick={this.handleClickSuccess}
          type="submit"
        />
        <IconButton
          text="Отмена"
          title="Отмена"
          onClick={this.handleClickCancel}
        />
      </form>
    </div>
  }

}
