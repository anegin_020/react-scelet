import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import IconButton from './IconButton';

export default class StyleButton extends PureComponent {

  static propTypes = {
    onToggle: PropTypes.func,
    style: PropTypes.string,
    label: PropTypes.string,
    isActive: PropTypes.bool,
  }

  onToggle = () => {
    this.props.onToggle(this.props.style)
  }

  render() {
    const {label, isActive, style} = this.props
    return (
      <IconButton
        onClick={this.onToggle}
        isActive={isActive}
        text={label}
        title={label}
        icon={style}
      />
    )
  }
}
