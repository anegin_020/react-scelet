import React, {PureComponent} from 'react'
import {Link} from 'react-router-dom'
import '../../styles/answerComment.css'

export default class AnswerComment extends PureComponent {

  constructor(props) {
    super(props)
    this.state = {
      senderUrl: '',
      senderThumb: '',
      nickname: '',
      readOnly: false,
    }
  }

  componentWillMount() {
    const data = this.props.contentState.getEntity(this.props.block.getEntityAt(0)).getData()
    this.setState({
      senderUrl: data.senderUrl,
      senderThumb: data.senderThumb,
      nickname: data.nickname,
      readOnly: this.props.blockProps.getReadOnly()
    })
  }

  shouldComponentUpdate(nextProps, nextState){
    return this.state.readOnly !== this.props.blockProps.getReadOnly()
  }

  componentWillReceiveProps(){
    this.setState({
      readOnly: this.props.blockProps.getReadOnly()
    })
  }

  renderAnswer = () => {
    const {senderUrl, senderThumb, nickname, readOnly} = this.state
    if(readOnly == true){
      return <Link className="rt_answerComment-sender" to={senderUrl}>
        <img src={senderThumb} alt={nickname}/>
        <span className="rt_answerComment-sender-nick">
            {nickname},
          </span>
      </Link>
    }
    return <span className="rt_answerComment-sender" style={{userSelect: 'none'}}>
      <img src={senderThumb} alt={nickname}/>
      <span className="rt_answerComment-sender-nick">
            {nickname},
          </span>
    </span>
  }

  render() {
    return (
      <div className={`rt_answerComment`}>
        {this.renderAnswer()}
      </div>
    )
  }

}
