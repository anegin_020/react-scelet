import React, {PureComponent} from 'react'
import {Link} from 'react-router-dom'
import moment from 'moment'
import RichEditComponent from '../../RichEditorComment'
import '../../styles/blockquouteComment.css'

export default class BlockquoteComment extends PureComponent {

  constructor(props) {
    super(props)
    moment.locale('ru')
    this.state = {
      select: false,
      isHidden: undefined,
      senderUrl: '',
      senderThumb: '',
      urlComment: '',
      nickname: '',
      dateComment: '',
      quote: undefined,
      readOnly: false,
      theme: {
        bgColor: "#ffffff"
      },
    }
  }

  componentWillMount() {
    const data = this.props.contentState.getEntity(this.props.block.getEntityAt(0)).getData()
    this.setState({
      senderUrl: data.senderUrl,
      senderThumb: data.senderThumb,
      urlComment: data.urlComment,
      nickname: data.nickname,
      dateComment: data.dateComment,
      editorQuote: RichEditComponent.createWithRowContent(data.quote),
      theme: this.props.blockProps.theme,
      readOnly: this.props.blockProps.getReadOnly()
    })
  }

  componentDidMount() {
    this.setState({
      isHidden: this.refs['rt_blockquouteComment-quote-body'].offsetHeight > 55 ? true : undefined,
    })
  }

  componentWillReceiveProps(){
    this.setState({
      readOnly: this.props.blockProps.getReadOnly()
    })
  }

  shouldComponentUpdate(nextProps, nextState){
    return nextState.isHidden === undefined || this.state.isHidden !== nextState.isHidden || this.state.select !== nextState.select || this.state.readOnly !== this.props.blockProps.getReadOnly()
  }

  componentWillUnmount(){
    if(this.state.select == true){
      this.removeListener()
    }
  }

  deleteByKey = (event) => {
    if(event.keyCode == 8 || event.keyCode == 46){
      this.props.blockProps.deleteBlockByKey(this.props.block.key)
      event.preventDefault()
      event.stopPropagation()
      return false
    }
  }

  closeByClick = (event) => {
    if(this.refs['rt_blockquouteComment-edit'] != event.target){
      this.removeListener()
      this.setState({select: false})
    }
  }

  handleEvent(event) {
    switch (event.type) {
      case 'keydown':
        ::this.deleteByKey(event)
        break
      case 'mousedown':
        ::this.closeByClick(event)
        break
      case 'touchstart':
        ::this.closeByClick(event)
        break
    }
  }

  removeListener = () => {
    document.removeEventListener('keydown', this, true);
    document.removeEventListener('touchstart', this, true);
    document.removeEventListener('mousedown', this, true);
  }

  handleClick = () => {
    this.setState((state) => ({
      isHidden: !state.isHidden
    }))
  }

  handleChangeEditorQuote = (editorQuote) => {
    this.setState({editorQuote})
  }

  handleClickEdit = () => {
    if(this.state.select == false){
      document.addEventListener('keydown', this, true);
      document.addEventListener('touchstart', this, true);
      document.addEventListener('mousedown', this, true);
      this.setState({select: true})
    }
  }

  optionHideClick = () => {
    if(this.state.isHidden !== undefined){
      return {['onClick']: this.handleClick}
    }
    return {}
  }

  render() {
    const {senderUrl, senderThumb, urlComment, nickname, dateComment, editorQuote, theme, isHidden, readOnly, select} = this.state
    return (
      <div className={`rt_blockquouteComment`}>
        {readOnly === false && <div
          ref="rt_blockquouteComment-edit"
          className={`rt_blockquouteComment-edit${select == true ? ' rt_blockquouteComment-edit-select' : ''}`}
          onClick={this.handleClickEdit}
        />}
        <Link className="rt_blockquouteComment-sender" to={urlComment}>
          {senderThumb && <img src={senderThumb} alt={nickname}/>}
          <span className="rt_blockquouteComment-sender-nick">
            {nickname}
          </span>
          <span className="rt_blockquouteComment-sender-date">
            {moment(dateComment, "YYYY-MM-DD HH:mm:ss").format("DD MMMM YYYY в kk:mm")}
          </span>
        </Link>
        <div className="rt_blockquouteComment-quote-body" ref="rt_blockquouteComment-quote-body"
             {...this.optionHideClick()}
        >
          <span className="NIcon NIcon-quote-comment" />
          {editorQuote &&
          <div
            className={`${isHidden === undefined
              ? ''
              : isHidden == true
                ? ' rt_blockquouteComment-quote-hidden'
                : ' rt_blockquote-quote-view'}`}
          >
            {isHidden == true && readOnly === true && <div
              className="rt_blockquouteComment-quote-substrate"
              style={{background: `linear-gradient(to bottom, rgba(0,0,0,0) 0%,${theme.bgColor} 100%)`}}
            />}
            <RichEditComponent
              className="rt_blockquouteComment-rt"
              readOnly={true}
              inQuote={true}
              onChange={this.handleChangeEditorQuote}
              editorState={editorQuote}
            />
          </div>}
        </div>
      </div>
    )
  }

}
