import React, {PureComponent} from 'react'
import '../../styles/imageComponent.css'

export function ImageComponent({src, title, children}){
  return (
    <div className={`rt_imageComment`}>
      <div>
        <img src={src} alt={title}/>
      </div>
      {title !== undefined && <span>{title}</span>}
      {children}
    </div>
  )
}

export default class ImageComponentHOC extends PureComponent {

  render() {
    const data = this.props.contentState.getEntity(this.props.block.getEntityAt(0)).getData();
    return <ImageComponent src={data.src} title={data.title}/>
  }

}
