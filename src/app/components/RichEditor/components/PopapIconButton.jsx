import React, {PureComponent} from 'react'
import PropTypes from 'prop-types'
import IconButton from './IconButton'

export default class PopapIconButton extends PureComponent{

  static propTypes = {
    onEditShowLink: PropTypes.func,
    label: PropTypes.string,
    wrapper: PropTypes.node,
    isActive: PropTypes.bool,
    isDisabled: PropTypes.bool,
    isShowPopapLink: PropTypes.bool,
  }

  render(){
    const {isDisabled, isActive, label, onEditShowLink, wrapper} = this.props;
    return <IconButton
      text={label}
      title={label}
      isDisabled={isDisabled}
      isActive={isActive}
      onClick={onEditShowLink}
      wrapper={wrapper}
    />
  }

}
