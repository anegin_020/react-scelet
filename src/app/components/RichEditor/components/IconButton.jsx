import React, {PureComponent} from 'react'
import PropTypes from 'prop-types'
import {getIconClassByStyle} from '../utils/helper'
import '../styles/iconButton.css'

export default class IconButton extends PureComponent{

  static propTypes = {
    type: PropTypes.string,
    title: PropTypes.string,
    text: PropTypes.string,
    icon: PropTypes.string,
    iconName: PropTypes.string,
    onClick: PropTypes.func,
    isActive: PropTypes.bool,
    isDisabled: PropTypes.bool,
    wrapper: PropTypes.node,
  }

  static defaultProps = {
    type: 'button',
    text: null,
    wrapper: null,
  }

  handleMouseDown = (e) => {
    e.preventDefault();
    this.props.onClick()
  }

  render() {
    const {type, title, text, isActive, isDisabled, wrapper, icon} = this.props
    let disabled = {}
    if(isDisabled){
      disabled = {disabled: 'disabled'}
    }
    return (
      <div className="rt_icon_button-wrapper">
        <button
          type={type}
          {...disabled}
          title={title}
          className={`rt_icon_button${isActive ? ' rt_icon_button-active' : ''}${isDisabled ? ' rt_icon_button-disabled' : ''}`}
          onMouseDown={this.handleMouseDown}
        >
          {!icon
            ? text
            : <span className={`NIcon ${getIconClassByStyle(icon)}`} />
          }
        </button>
        {wrapper}
      </div>
    )
  }

}
