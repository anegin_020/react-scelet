import {EditorState, convertFromRaw, convertFromHTML, convertToRaw, ContentState, CompositeDecorator, Modifier, genKey, CharacterMetadata, ContentBlock, SelectionState} from 'draft-js'

export const helperCreateWithRowContent = (rowContent , newDecorators) => {
  /*forEach(rowContent.entityMap, function(value, key) {
   value.data.mention = fromJS(value.data.mention)
   })*/
  const contentState = convertFromRaw(rowContent)
  if(newDecorators == undefined){
    return EditorState.createWithContent(contentState, new CompositeDecorator(decorators))
  }
  return EditorState.createWithContent(contentState, new CompositeDecorator(decorators.concat(newDecorators)))
}

export const helperCreateWithHTMLContent = (htmlContent, newDecorators) => {
  const blocksFromHTML = convertFromHTML(htmlContent);
  const state = ContentState.createFromBlockArray(
    blocksFromHTML.contentBlocks,
    blocksFromHTML.entityMap,
  );
  if(newDecorators == undefined){
    return EditorState.createWithContent(state, new CompositeDecorator(decorators))
  }
  return EditorState.createWithContent(state, new CompositeDecorator(decorators.concat(newDecorators)))
}

export const helperCreateEmptyContent = (newDecorators) => {
  if(newDecorators == undefined){
    return EditorState.createEmpty(new CompositeDecorator(decorators))
  }
  return EditorState.createEmpty(new CompositeDecorator(decorators.concat(newDecorators)))
}

export const helperRenderContentToRow = (currentState) => {
  return convertToRaw(currentState)
}

export const helperRenderContentToHTML = (currentState) => {
  return convertToHTML({
    blockToHTML: blockToHTML,
    entityToHTML: entityToHTML,
  })(currentState)
}
