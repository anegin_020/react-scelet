export const checkLinkReact = (url) => (
  !!url.match(new RegExp(`(^\/\/|^\/|https?://${window.location.hostname})`))
)
