import {fork, all} from 'redux-saga/effects';
import AppSagas from '../containers/App/sagas';
import ImageSagas from './image';

export default function* root() {
    yield all([
        //fork(ImageSagas.workers)
    ]);
    yield all([
        fork(AppSagas.init),
    ]);
}
