import { takeEvery, takeLatest, select, call, put } from 'redux-saga/effects';
import ImageConstants from '../../constants/ImageConstants';
import {loadRandomImageService} from '../../services/ImageServices';
import {getImageApiKey} from '../../reducers/selectors';

function* loadImage() {
    try {
        const apiKey = yield select(getImageApiKey);
        const images = yield call(loadRandomImageService, apiKey);
        yield put({type: ImageConstants.GET_RANDOM_IMAGE, payload: images});
    } catch (e) {
        yield put({type: ImageConstants.GET_RANDOM_IMAGE_FAILURE, error: e.message});
    }
}

export function* getRandomImageSaga() {
    yield takeEvery(ImageConstants.GET_RANDOM_IMAGE_REQUEST, loadImage);
}