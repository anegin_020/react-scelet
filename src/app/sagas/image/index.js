import SagasModel from '../SagasModel';
import * as ImageSagas from './ImageSagas';

export default new SagasModel({
    workers: ImageSagas.getRandomImageSaga
});