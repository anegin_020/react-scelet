import {fork} from 'redux-saga/effects';
import SagasModel from '../SagasModel';
import * as AppSagas from './AppSagas';

export default new SagasModel({
    init: function* (){
        yield [
            fork(AppSagas.appInitSaga)
        ];
    },
});