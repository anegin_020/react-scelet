export default class SagasModel {
    constructor({init, workers}){
        if(init){
            this.init = init;
        }
        if(workers){
            this.workers = workers;
        }
    }
    init = null;
    workers = null;
}