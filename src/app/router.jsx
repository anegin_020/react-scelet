import React from 'react';
import {Route, Switch} from 'react-router-dom';
import App from './containers/App';
import Home from './containers/Home/index-rich';

import { getAsyncInjectors } from './utils/asyncInjectors';

const errorLoading = (err) => {
  console.error('Dynamic page loading failed', err); // eslint-disable-line no-console
};

const loadModule = (cb) => (componentModule) => {
  cb(null, componentModule.default);
};

export default (store) => {
  /*const { injectReducer, injectSagas } = getAsyncInjectors(store);
  const router = [
    {
      path: '/',
      exact: true,
      getComponent(nextState, cb) {
        const importModules = Promise.all([
          import('./containers/Home/reducer'),
          import('./containers/Home/sagas'),
          import('./containers/Home'),
        ]);

        const renderRoute = loadModule(cb);

        importModules.then(([reducer, sagas, component]) => {
          injectReducer('home', reducer.default);
          injectSagas(sagas.default);

          renderRoute(component);
        });

        importModules.catch(errorLoading);
      },
    },
  ];*/

  return (
    <App>
      <Switch>
        {/*{childRoutes: router}*/}
        <Route exact path="/" component={Home}/>
      </Switch>
    </App>
  )
}
