require('dotenv').config();
var join = require("path").join;
var webpack = require("webpack");
var ExtractTextPlugin = require("extract-text-webpack-plugin");
var CopyWebpackPlugin = require('copy-webpack-plugin');
var HtmlPlugin = require('html-webpack-plugin');

module.exports = {
  devtool: 'inline-source-map',
  entry: [
    'webpack-hot-middleware/client?reload=true',
    'whatwg-fetch',
    'babel-polyfill',
    join(__dirname, '..', '..', 'src', 'app', 'index'),
  ],
  output: {
    path: join(__dirname),
    filename: 'bundle.js',
    publicPath: '/',
  },
  resolve: {
    extensions: ['.js', '.jsx']
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new webpack.LoaderOptionsPlugin({
      minimize: false,
      debug: true,
    }),
    new HtmlPlugin({
      filename: 'index.html',
      template: join(__dirname, '..', '..', 'src', 'html', 'index.ejs')
    }),
    new webpack.NoEmitOnErrorsPlugin(),
    new webpack.ProvidePlugin({
      'Promise': 'es6-promise',
    }),
    new webpack.DefinePlugin({
      "process.env": {
        NODE_ENV: JSON.stringify("development"),
      }
    }),
    new CopyWebpackPlugin([
        {
          context: join(__dirname, '..', '..', 'src', 'assets', 'img'),
          from: '**/*',
          to: join(__dirname, 'assets', 'img')
        },
        {
          context: join(__dirname, '..', '..', 'src', 'assets', 'fonts'),
          from: '**/*',
          to: join(__dirname, 'assets', 'fonts')
        },
        {
          context: join(__dirname, '..', '..', 'src', 'assets', 'css'),
          from: '**/*',
          to: join(__dirname, 'assets', 'style')
        },
      ], {copyUnmodified: false}
    ),
  ],
  module: {
    rules: [
      /* {
       test: /.*!/,
       include: [join(__dirname, '..', '..', 'src', 'app', 'containers', 'pages/common')],
       use: [
       {
       loader: 'bundle-loader',
       options: {lazy: true, name: 'common'}
       }
       ]
       },*/
      {
        test: /\.jsx?/,
        exclude: [/node_modules/],
        use: ['babel-loader']
      },
      {
        test: /\.css/,
        exclude: /node_modules/,
        use: [
          {
            loader: 'style-loader',
            options: {sourceMap: true}
          },
          {
            loader: 'css-loader',
            options: {importLoaders: 1},
          },
          'resolve-url-loader',
        ]
      },
      {
        test: /(plugin\.css|Draft\.css)$/,
        use: [
          {
            loader: 'style-loader',
            options: {sourceMap: true}
          },
          {
            loader: 'css-loader',
            options: {importLoaders: 1},
          },
          'resolve-url-loader',
        ]
      },
      {
        test: /\.(png|jpg|gif|ttf|eot|svg|woff(2)?)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        use: {
          loader: "file-loader",
          query: {
            name: '[name].[ext]?[hash]',
            limit: '10000'
          }
        }
      },
      {
        test: /\.json$/,
        use: ['json-loader'],
      }
    ],
  }
};
