require('dotenv').config();
var join = require("path").join;
var webpack = require("webpack");
var ExtractTextPlugin = require("extract-text-webpack-plugin");
var CopyWebpackPlugin = require('copy-webpack-plugin');
var CleanWebpackPlugin = require('clean-webpack-plugin');
var CompressionPlugin = require("compression-webpack-plugin");
var HtmlPlugin = require('html-webpack-plugin');
var autoprefixer = require('autoprefixer');

module.exports = {
    entry: [
        'whatwg-fetch',
        'babel-polyfill',
        join(__dirname, '..', '..', 'src', 'app', 'index'),
    ],
    output: {
        path: join(__dirname, '..', '..', 'bundle'),
        filename: 'assets/js/[name].js?[hash]',
        publicPath: '/'
    },
    resolve: {
        extensions: ['.js', '.jsx']
    },
    plugins: [
        new CleanWebpackPlugin(['bundle'], {
            root: join(__dirname, '..', '..'),
            verbose: true,
            dry: false,
        }),
        new webpack.LoaderOptionsPlugin({
            minimize: true,
            debug: false,
        }),
        new HtmlPlugin({
            filename: "/index.html",
            template: join(__dirname, '..', '..', 'src', 'html', 'index.ejs')
        }),
        new ExtractTextPlugin('assets/style/style.css'),
        new webpack.ProvidePlugin({
            'Promise': 'es6-promise',
        }),
        new webpack.DefinePlugin({
            "process.env": {
                NODE_ENV: JSON.stringify("production"),
            }
        }),
        new webpack.optimize.UglifyJsPlugin({
            acorn: true,
            output: {comments: false},
            sourceMap: false,
            compress: {
                warnings: false
            }
        }),
        new CopyWebpackPlugin([
                {
                    context: join(__dirname, '..', '..', 'src', 'assets', 'img', 'ready'),
                    from: '**/*',
                    to: join(__dirname, '..', '..', 'bundle', 'assets', 'img')
                },
                {
                    context: join(__dirname, '..', '..', 'src', 'assets', 'fonts'),
                    from: '**/*',
                    to: join(__dirname, '..', '..', 'bundle', 'assets', 'fonts')
                },
                {
                    context: join(__dirname, '..', '..', 'src', 'assets', 'css'),
                    from: '**/*',
                    to: join(__dirname, '..', '..', 'bundle', 'assets', 'style')
                },
            ], {copyUnmodified: false}
        ),
        new CompressionPlugin({
            asset: "[path].gz?[query]",
            algorithm: "gzip",
            test: /\.js|\.css$|\.html$/,
            threshold: 10240,
            minRatio: 0.8
        }),
        new webpack.LoaderOptionsPlugin({
            options: {
                postcss: [
                    autoprefixer({
                        browsers: [
                            '>1%',
                            'last 4 versions',
                            'Firefox ESR',
                            'not ie < 9', // React doesn't support IE8 anyway
                        ]
                    }),
                ]
            }
        })
    ],
    module: {
        rules: [
            {
                test: /.*/,
                include: [join(__dirname, '..', '..', 'src', 'app', 'containers', 'pages', 'common')],
                use: [
                    {
                        loader: 'bundle-loader',
                        options: {lazy: true, name: 'common'}
                    }
                ]
            },
            {
                test: /\.jsx?/,
                exclude: [/node_modules/],
                use: ['babel-loader']
            },
            {
                test: /\.css/,
                exclude: /node_modules/,
                use: ExtractTextPlugin.extract({
                    fallback: "style-loader",
                    use: [
                        {
                            loader: 'css-loader',
                            options: {postcss: true, importLoaders: 1},
                        },
                        'postcss-loader',
                        'resolve-url-loader',
                    ]
                })
            },
            {
                test: /\.(png|jpg|gif|ttf|eot|svg|woff(2)?)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                use: {
                    loader: "file-loader",
                    query: {
                        name: '[name].[ext]?[hash]',
                        limit: '10000'
                    }
                }
            }
        ],
    },
};