require('dotenv').config();
const
    express = require('express'),
    argv = require('minimist')(process.argv.slice(2)),
    join = require('path').join,
    setup = require('./middlewares/frontendMiddleware'),
    app = express();
console.log("Lalka",process.env.NODE_ENV, process.env.HOST);
setup(app, {
    outputPath: join(__dirname, '..', 'build'),
    publicPath: '/',
});

const host = argv.host || process.env.HOST || null;
const port = argv.port || process.env.PORT || 3000;

app.listen(port, host, (err) => {
    if(err){
        console.log(err);
    }
});
