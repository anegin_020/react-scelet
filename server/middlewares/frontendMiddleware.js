require('dotenv').config();
const
    express = require('express'),
    join = require("path").join,
    DashboardPlugin = require('webpack-dashboard/plugin'),
    compression = require('compression');

// Dev middleware
const addDevMiddlewares = (app, webpackConfig) => {
    const
        webpack = require('webpack'),
        webpackDevMiddleware = require('webpack-dev-middleware'),
        webpackHotMiddleware = require('webpack-hot-middleware'),
        compiler = webpack(webpackConfig);

    compiler.apply(new DashboardPlugin());

    const middleware = webpackDevMiddleware(compiler, {
        noInfo: true,
        publicPath: webpackConfig.output.publicPath,
        silent: true,
        stats: 'errors-only',
    });

    app.use(middleware);
    app.use(webpackHotMiddleware(compiler));

    const fs = middleware.fileSystem;

    app.get('*', (req, res) => {
        fs.readFile(join(compiler.outputPath, 'index.html'), (err, file) => {
            if (err) {
                res.sendStatus(404);
            } else {
                res.send(file.toString());
            }
        });
    });
};

// Production middlewares
const addProdMiddlewares = (app, options) => {
    const publicPath = options.publicPath || '/';
    const outputPath = options.outputPath || join(__dirname, '..', '..', 'build');

    app.use(compression());
    app.use(publicPath, express.static(outputPath));

    app.get('*', (req, res) => res.sendFile(join(outputPath, 'index.html')));
};

/**
 * Front-end middleware
 */
module.exports = (app, options) => {
    const isProd = process.env.NODE_ENV === 'production';

    if (isProd) {
        addProdMiddlewares(app, options);
    } else {
        const webpackConfig = require('../configs/webpack.config.dev');
        addDevMiddlewares(app, webpackConfig);
    }

    return app;
};